<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\web\Link;
use yii\helpers\Url;
use yii\web\Linkable;

class Usecase extends ActiveRecord implements Linkable
{
    public $imageFile;
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{usecase}}';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'image'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Подпись',
            'image' => 'Изображение',
        ];
    }

    public function upload()
    {
        if ($this->validate() && isset($this->imageFile)) {
            $this->imageFile->saveAs('themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = 'themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    public static function deleteItem($id)
    {
        $case = Usecase::findOne(['id' => $id]);
        return ($case->delete()) ? true : false;
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['rest/usecases/' . $this->id], true),
            Link::REL_LIST => Url::to(['rest/usecases/'], true),
        ];
    }
}