<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [
        //rbac
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        //Google reCaptcha
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'pixelycia\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LctdjAUAAAAABbjm0UzknCMA0QytvKYG2cVuCaK',
            'secret' => '6LctdjAUAAAAADcmBR2rk5S2WGMMgWYYPO6v5gaS',
        ],

        //IP Geo
        'ipgeobase' => [
            'class' => 'himiklab\ipgeobase\IpGeoBase',
            'useLocalDB' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'veryverysecretkey',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
//            'enableSession' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],


        //theming
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/default',
                'baseUrl' => '@web/themes/default',
                'pathMap' => [
                    '@app/views' => '@app/themes/default',
                    '@app/modules' => '@app/themes/default/modules',
                    '@app/widgets' => '@app/themes/default/widgets',

                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/bonus'
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/cake',
                    'extraPatterns' => [
                        'POST add-image' => 'add-image',
                        'DELETE delete-image' => 'delete-image',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/order'
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/product',
                    'extraPatterns' => [
                        'POST add-bonus' => 'add-bonus',
                        'POST add-cake' => 'add-cake',
                        'DELETE delete-bonus' => 'delete-bonus',
                        'DELETE delete-cake' => 'delete-cake',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/usecase'
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'rest/user',
                    'except' => ['delete']
                ],
                '' => 'site/index',
                'cms' => '/cms/backend/index',
                'cms/login' => '/cms/backend/login',
                'cms/logout' => 'cms/backend/logout',
                'cms/access-deny' => '/cms/backend/access-deny',

                'cms/products/<action:[\w\-]+>/<id:\d+>' => 'cms/backend/<action>',
                'cms/products/<action:[\w\-]+>' => 'cms/backend/<action>',
                'cms/products' => 'cms/backend/products',
                'cms/products/add-cake' => 'cms/backend/addCake',
                'cms/products/delete-cake' => 'cms/backend/deleteCake',
                'cms/products/delete-bonus' => 'cms/backend/deleteBonus',
                'cms/products/delete-product' => 'cms/backend/deleteProduct',
                'cms/cakes/delete-image' => 'cms/cake/delete-image',
                'cms/cakes/<action:[\w\-]+>/<id:\d+>' => 'cms/cake/<action>',
                'cms/cakes/<action:[\w\-]+>' => 'cms/cake/<action>',
                'cms/bonuses' => 'cms/bonus/index',
                'cms/bonuses/<action:[\w\-]+>/<id:\d+>' => 'cms/bonus/<action>',
                'cms/bonuses/<action:[\w\-]+>' => 'cms/bonus/<action>',
                'cms/<controller:[\w\-]+>s' => 'cms/<controller>/index',
                'cms/<controller:[\w\-]+>s/<action:[\w\-]+>/<id:\d+>' => 'cms/<controller>/<action>',
                'cms/<controller:[\w\-]+>s/<action:[\w\-]+>/<name:[\w\-]+>' => 'cms/<controller>/<action>',
                'cms/<controller:[\w\-]+>s/<action:[\w\-]+>' => 'cms/<controller>/<action>',
                '<controller:[\w\-]+>s' => '<controller>/index',

            ],
        ],

        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
