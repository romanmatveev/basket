<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cake_image`.
 */
class m170830_063129_create_cake_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cake_image', [
            'id' => $this->primaryKey(),
            'cake_id' => $this->integer()->notNull(),
            'image' => $this->string()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cake_image');
    }
}
