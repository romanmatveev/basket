<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\web\Linkable;
use yii\helpers\Url;
use yii\web\Link;

/**
 * Class Cake таблица cake
 * @property integer $id
 * @property string  $name
 * @property string  $composition
 * @property string  $description
 * @package app\models
 */
class Cake extends ActiveRecord implements Linkable
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{cake}}';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'composition'], 'required'],
            ['name', 'string'],
            ['composition', 'string', 'min' => 3],
            ['description', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'composition' => 'Состав',
            'description' => 'Описание',
        ];
    }

    public function getFirstImage($id)
    {
        $image = CakeImage::find()
            ->where(['cake_id' => $id])
            ->one();
        return $image->image;
    }

    public function getCakeImages()
    {
        return $this->hasMany(CakeImage::className(), ['cake_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_cake', ['cake_id' => 'id']);
    }

    public function getProductCake()
    {
        return $this->hasMany(ProductCake::className(), ['cake_id' => 'id']);
    }

    public static function deleteItem($id)
    {
        $cake = Cake::findOne(['id' => $id]);
        return ($cake->delete()) ? true : false;
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['rest/cakes/' . $this->id], true),
            Link::REL_LIST => Url::to(['rest/cakes/'], true),
        ];
    }
}