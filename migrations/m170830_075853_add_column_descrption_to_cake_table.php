<?php

use yii\db\Migration;

class m170830_075853_add_column_descrption_to_cake_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cake', 'description', $this->string(2500));
    }

    public function safeDown()
    {
        $this->dropColumn('cake', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170830_075853_add_column_descrption_to_cake_table cannot be reverted.\n";

        return false;
    }
    */
}
