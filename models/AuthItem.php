<?php

namespace app\models;


use yii\db\ActiveRecord;

class AuthItem extends ActiveRecord
{
    public static function tableName()
    {
        return '{{auth_item}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['name', 'description'], 'string'],
            ['type', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'type' => 'Тип',
            'description' => 'Описание',
        ];
    }

    public function getChild()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    public function getParent()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    public static function deleteItem($name)
    {
        AuthAssignment::deleteAll(['item_name' => $name]);
        AuthItemChild::deleteAll(['parent' => $name]);
        AuthItemChild::deleteAll(['child' => $name]);
        $item = AuthItem::findOne(['name' => $name]);
        if ($item->delete()) {
            return true;
        } else {
            return false;
        }
    }
}