<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Order таблица order
 * @property integer $id
 * @property integer $product_id
 * @property integer $bonus_id
 * @property string  $username
 * @property string  $phone
 * @property string  $email
 * @property string  $address
 * @property string  $comment
 * @property boolean $present
 * @property integer $total
 * @package app\models
 */
class Order extends ActiveRecord {

//    public $insert = true;
    public $reCaptcha;
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{orders}}';
    }

    /**
    * @return array the validation rules.
    */
    public function rules()
    {
        return [
            [['username', 'phone', 'address', 'email'], 'required', 'message' => 'Необходимо заполнить {attribute}'],
            ['email', 'email'],
            ['username', 'string', 'min' => 4, 'max' => 50],
            ['address', 'string', 'max' => 100],
            ['comment', 'string', 'max' => 250],
            ['phone', 'match', 'pattern' => '/((8|\+7)(\-|\ )?)?\(?\d{3}\)?(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}$/'],
            [['product_id', 'bonus_id', 'total'], 'integer'],
            ['present', 'boolean'],
            [['reCaptcha'], \pixelycia\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LctdjAUAAAAADcmBR2rk5S2WGMMgWYYPO6v5gaS'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'ФИО',
            'phone' => 'Номер телефона',
            'address' => 'Адрес',
            'email' => 'E-mail',
            'comment' => 'Комментарий',
            'total' => 'Итоговая стоимость',
            'present' => 'Подарочная упаковка',
            'product_id' => 'product_id',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $model = Order::find()->orderBy(['id' => SORT_DESC])->one();

        $text = 'Dear ' . $model->username . '!/nYour order in process. We will deliver it at 00:00 to ' . $model->address;

        Yii::$app->mailer->compose()
            ->setTo($model->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Order')
            ->setTextBody($text)
            ->send();

        $text = 'New order from ' . $model->username . ' this:/n phone: ' . $model->phone . '/n mail: ' . $model->email . '/n address: ' . $model->address . '/n comment: ' . $model->comment;

        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom($model->email)
            ->setSubject('New order')
            ->setTextBody($text)
            ->send();
    }

    public function getProducts()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getBonuses()
    {
        return $this->hasOne(Bonus::className(), ['id' => 'bonus_id']);
    }
}