<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Пользователи';

?>
    <div class="container">
        <?php if (Yii::$app->session->hasFlash('deleted')): ?>
            <p class="alert-danger">Объект удален.</p>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
            <p class="alert-danger">Ошибка при удалении!</p>
        <?php endif; ?>
        <h4>Пользователи</h4>
        <div class="row">
            <div class="col-md-6 blog-main">
                <div class="blog-post">
                    <div class="blog-post">
                        <table class="table">
                            <tbody>
                            <?php foreach ($users as $user) { ?>
                                <?php if (\Yii::$app->user->can('updateUser', ['profileId' => $user->id])) { ?>
                                <tr>
                                    <td>
                                            <?= Html::a(Html::encode($user->username), Url::to(['/cms/user/view', 'id' => $user->id])) ?>
                                    </td>
                                        <td>
                                            <?= Html::a('Изменить', Url::to(['/cms/user/view', 'id' => $user->id]), ['class' => 'btn btn-primary']) ?>
                                        </td>

                                    <?php if (\Yii::$app->user->can('deleteUser')) { ?>
                                        <td>
                                            <?= Html::a('Удалить', Url::to(['/cms/user/delete', 'id' => $user->id]), ['class' => 'btn btn-danger']) ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('createUser')) { ?>
                                <tr>
                                    <td>
                                        <?= Html::a('Добавить', Url::to(['/cms/user/view']), ['class' => 'btn btn-success']) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php if (\Yii::$app->user->can('readRole')) { ?>
    <div class="container">
        <h4>Роли</h4>
        <div class="row">
            <div class="col-md-6 blog-main">
                <div class="blog-post">
                    <div class="blog-post">
                        <table class="table">
                            <tbody>
                            <?php foreach ($roles as $role) { ?>
                                <tr>
                                    <td>
                                        <?php if (\Yii::$app->user->can('updateRole')) { ?>
                                            <?= Html::a(Html::encode($role->name), Url::to(['/cms/role/view', 'name' => $role->name])) ?>
                                        <?php } else {
                                            echo Html::encode($role->name);
                                        } ?>
                                    </td>
                                    <td>
                                        <?php if (\Yii::$app->user->can('updateRole')) { ?>
                                            <?= Html::a('Изменить', Url::to(['/cms/role/view', 'name' => $role->name]), ['class' => 'btn btn-primary']) ?>
                                        <?php } ?>
                                    </td>
                                    <?php if (\Yii::$app->user->can('deleteRole')) { ?>
                                        <td>
                                            <?= Html::a('Удалить', Url::to(['/cms/role/delete', 'name' => $role->name]), ['class' => 'btn btn-danger']) ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('createRole')) { ?>
                                <tr>
                                    <td>
                                        <?= Html::a('Добавить', Url::to(['/cms/role/view']), ['class' => 'btn btn-success']) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>