<?php

namespace app\controllers\rest;

use app\models\Usecase;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use app\models\User;
use yii\filters\AccessControl;
use yii;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class UsecaseController extends ActiveController
{
    public $modelClass = 'app\models\Usecase';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if ($user->validatePassword($password)) {
                    return $user;
                }
            },
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['apiPermission'],
                ],
            ],
            'denyCallback' => function () {
                throw new yii\web\NotAcceptableHttpException('Not enough permissions for this operations');
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new Usecase();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->image) {
            $putdata = fopen("file:///" . $model->image, "r");

            $short_path = "themes/default/img/" . basename($model->image);
            $path = \yii::getAlias('@webroot/') . $short_path;

            $fp = fopen($path, "w");

            while ($data = fread($putdata, 1024))
                fwrite($fp, $data);

            fclose($fp);
            fclose($putdata);

            $model->image = $short_path;
        }
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/bonuses/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = Usecase::findOne(['id' => $id]);
        $old_image = $model->image;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->image != $old_image) {
            $putdata = fopen("file:///" . $model->image, "r");

            $short_path = "themes/default/img/" . basename($model->image);
            $path = \yii::getAlias('@webroot/') . $short_path;

            $fp = fopen($path, "w");

            while ($data = fread($putdata, 1024))
                fwrite($fp, $data);

            fclose($fp);
            fclose($putdata);

            $model->image = $short_path;
        }
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(202);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/bonuses/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionDelete($id)
    {
        if (Usecase::deleteItem($id)) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(204);
            $response->getHeaders()->set('Location', Url::to('@web/rest/bonuses'));
            return $response;
        } else {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }
}