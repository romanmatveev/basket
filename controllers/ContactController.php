<?php

namespace app\controllers;

use app\models\Order;
use app\models\Content;
use Yii;
use yii\web\Controller;

class ContactController extends Controller
{
    public function actionIndex()
    {
        $order = new Order();
        if ($order->load(Yii::$app->request->post()) && $order->save()) {
            return $this->refresh();
        }
        $content = new Content();
        $content = $content->getContent();
        return $this->render('index', compact('order', 'content'));
    }
}