<?php

namespace app\controllers\cms;

use app\models\AuthAssignment;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use app\models\User;
use app\models\AuthItem;
use yii\helpers\Url;

class UserController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'only' => ['index', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteUser'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionIndex()
    {
        $users = User::find()
            ->orderBy('username')
            ->all();
        $roles = AuthItem::find()
            ->where(['type' => 1])
            ->all();
        return $this->render('index', compact('users', 'roles'));
    }

    public function actionView($id = null)
    {
        if (isset($id)) {
            $user = User::findOne(['id' => $id]);
        } else {
            $user = new User();
        }

        $roles = AuthItem::find()
            ->select(['name'])
            ->where(['type' => 1])
            ->indexBy('name')
            ->column();

        if ($user->load(Yii::$app->request->post())) {
            if ($user->password == '') {
                $current_user = User::findByUsername($user->username);
                $user->password = $current_user->password;
            } else {
                $user->password = Yii::$app->getSecurity()->generatePasswordHash($user->password);
            }
            if ($user->save()) {
                if (isset($user->role)) {
                    if ($user->role->load(Yii::$app->request->post()) && $user->role->save())
                        Yii::$app->session->setFlash('userwaschanged');
                } else {
                    $role = new AuthAssignment();
                    $role->user_id = $user->id;
                    $role->item_name = 'reader';
                    $role->created_at = date('YmdH');
                    $role->save();

                    return $this->redirect(Url::to(['cms/user/view', 'name' => $user->id]));
                }
            }

        }

        return $this->render('view', compact('user', 'roles'));
    }

    public function actionDelete($id)
    {
        (User::deleteItem($id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/users');
    }
}