<?php

use yii\db\Migration;

class m170823_130942_createOrderToTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50),
            'phone' => $this->string(15),
            'email' => $this->string(50),
            'address' => $this->string(100),
            'comment' => $this->string(250),
        ]);
    }

    public function safeDown()
    {
        echo "m170823_130942_createOrderToTable cannot be reverted.\n";

        $this->dropTable('orders');
    }
}
