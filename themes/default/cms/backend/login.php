<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>

<div class="login_form_container col-md-2 col-md-offset-5">
    <h2 class="form-signin-heading"><?= Html::encode($this->title) ?></h2>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-signin'],

    ]); ?>

    <?= $form->field($user, 'username')->textInput(['autofocus' => true, 'class' => 'form-control']) ?>

    <?= $form->field($user, 'password')->passwordInput() ?>

    <?= $form->field($user, 'rememberMe')->checkbox([
        'class' => 'checkbox',
    ]) ?>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

    <?php ActiveForm::end(); ?>
</div>
