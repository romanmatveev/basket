<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Пользователь';

?>

<div class="container">
    <h4>Пользователь</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <div class="blog-post">
                    <?php if (Yii::$app->session->hasFlash('userwaschanged')): ?>
                        <p class="alert-danger">Изменения сохранены.</p>
                    <?php endif; ?>
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ],
                        'fieldConfig' => [
                            'template' => "{label}\n{input}\n{hint}\n{error}",
                        ],
                    ]) ?>
                    <div class="col-md-12">
                        <?= $form->field($user, 'username')->textInput()->label() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($user, 'password')->passwordInput(['value'=>''])->label() ?>
                    </div>
                    <?php if (\Yii::$app->user->can('readRole')) { ?>
                        <div class="col-md-12">Роль пользователя</div>
                        <div class="col-md-12">
                            <?php if (isset($user->role)) { ?>
                                <?= $form->field($user->role, 'item_name')->dropdownList($roles,
                                    [
                                        'class' => 'col-md-6'
                                    ])->label(false) ?><?php } ?>
                        </div>
                    <?php } ?>
                    <div class="">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                        &nbsp;&nbsp;
                        <?= Html::a('Back', Url::toRoute(["/cms/users/"]), ['class' => 'btn btn-info']) ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>