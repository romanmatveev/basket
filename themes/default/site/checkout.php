<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Корзинка сладостей';

?>
<?php Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'toggleButton' => ['label' => 'click me'],
]); ?>
<div class="modal">
    <div class="modal__out">
        <div class="modal__in">
            <div class="modal__win">
                <div class="modal__container">
                    <button class="modal__close"></button>
                    <div class="modal__content">
                        <div class="modal-checkout">
                            <h1>Оформить заказ</h1>
                            <?php $form = ActiveForm::begin([
                                'id' => 'order-form',
                                'options' => ['class' => 'contacts__form form--white'],
                            ]) ?>
                            <div class="field__item">
                                <?= $form->field($order, 'username', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('username'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'phone', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('phone'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'address', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('address'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'email', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('email'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'comment', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('comment'),
                                    ],
                                ])->textarea(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <button class="button button--l button--gradient contacts__buy">
                                <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
                            </button>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var body = document.getElementsByTagName('body')[0];
    body.style.overflow = 'hidden';
</script>

<div class="modal">
    <div class="modal__out">
<!--        <div class="modal__in">-->
<!--            <div class="modal__win">-->
<!--                <div class="modal__container">-->
<!--                    <button class="modal__close"></button>-->
<!--                    <div class="modal__content">-->
<!--                        <div class="modal-checkout">-->
<!--                            <h1>Оформить заказ</h1>-->
<!--                            --><?php //$form = ActiveForm::begin([
//                                'id' => 'order-form',
//                                'options' => ['class' => 'contacts__form form--white'],
//                            ]) ?>
<!--                            <div class="field__item">-->
<!--                                --><?//= $form->field($order, 'username', [
//                                    'inputOptions' => [
//                                        'placeholder' => $order->getAttributeLabel('username'),
//                                    ],
//                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
<!--                            </div>-->
<!--                            <div class="field__item">-->
<!--                                --><?//= $form->field($order, 'phone', [
//                                    'inputOptions' => [
//                                        'placeholder' => $order->getAttributeLabel('phone'),
//                                    ],
//                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
<!--                            </div>-->
<!--                            <div class="field__item">-->
<!--                                --><?//= $form->field($order, 'address', [
//                                    'inputOptions' => [
//                                        'placeholder' => $order->getAttributeLabel('address'),
//                                    ],
//                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
<!--                            </div>-->
<!--                            <div class="field__item">-->
<!--                                --><?//= $form->field($order, 'email', [
//                                    'inputOptions' => [
//                                        'placeholder' => $order->getAttributeLabel('email'),
//                                    ],
//                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
<!--                            </div>-->
<!--                            <div class="field__item">-->
<!--                                --><?//= $form->field($order, 'comment', [
//                                    'inputOptions' => [
//                                        'placeholder' => $order->getAttributeLabel('comment'),
//                                    ],
//                                ])->textarea(['class' => 'field__elem'])->label(false) ?>
<!--                            </div>-->
<!--                            <button class="button button--l button--gradient contacts__buy">-->
<!--                                --><?//= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
<!--                            </button>-->
<!--                            --><?php //ActiveForm::end() ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<?php Modal::end(); ?>