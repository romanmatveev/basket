<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii;
use yii\web\ServerErrorHttpException;

class CakeImage extends ActiveRecord
{
    public $imageFile;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{cake_image}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['cake_id', 'image'], 'string'],
            ['image', 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image' => 'Изображение',
        ];
    }

    public function getCakes()
    {
        return $this->hasOne(Cake::className(), ['id' => 'cake_id']);
    }

    public function upload()
    {
        if ($this->validate() && isset($this->imageFile)) {
            $this->imageFile->saveAs('themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = 'themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $file путь к файлу на HDD
     * @return string
     * @throws ServerErrorHttpException
     */
    public static function uploadFile($file)
    {
        $path = Yii::getAlias('@webroot/') . "themes/default/img/";
        $size = filesize($file);
        $part_size = 1048576;

        $amount = (int)ceil($size / $part_size);
        $source_file = fopen("file:///" . $file, "rb");
        $bufer = fread($source_file, $size);
        fclose($source_file);
        $source_crc = md5_file($file);
        for ($i = 0; $i < $amount; $i++) {
            $part = substr($bufer, $i * $part_size, $part_size);
            $file_part = fopen($path . basename($file) . $i . ".part", "wb");
            fwrite($file_part, $part);
            fclose($file_part);
        }
        $bufer = '';
        for ($i = 0; $i < $amount; $i++) {
            $filename = $path . basename($file) . $i . ".part";
            if (file_exists($filename)) {
                $file_part = fopen($filename, "rb");
                $bufer .= fread($file_part, filesize($filename));
                fclose($file_part);
                unlink($filename);
            }
        }
        $filename = $path . basename($file);
        $target_file = fopen($filename, "wb");
        fwrite($target_file, $bufer);
        fclose($target_file);
        $target_crc = md5_file($filename);
        $filename = Yii::getAlias('@web') . "themes/default/img/" . basename($file);
        if ($source_crc == $target_crc) {
            return $filename;
        } else {
            throw new ServerErrorHttpException('Failed to download the file for unknown reason.');
        }
    }
}