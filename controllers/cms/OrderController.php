<?php

namespace app\controllers\cms;

use app\models\Order;
use app\models\Content;
use yii\web\Controller;
use yii\base\Model;
use yii\helpers\Url;
use Yii;
use yii\filters\AccessControl;

class OrderController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['readOrder'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionIndex()
    {

        $orders = Order::find()
            ->orderBy('id')
            ->all();
        return $this->render('index', compact('orders'));
    }
}