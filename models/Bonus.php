<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\Linkable;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\web\Link;

/**
 * Class Bonus таблица bonus
 * @property integer $id
 * @property string  $name
 * @property string  $image
 * @package app\models
 */
class Bonus extends ActiveRecord implements Linkable
{
    public $imageFile;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{bonus}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'image'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'  => 'Название',
            'image' => 'Изображение',
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_bonus', ['bonus_id' => 'id']);
    }

    public function getProductBonus()
    {
        return $this->hasMany(ProductBonus::className(), ['bonus_id' => 'id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['bonus_id' => 'id']);
    }

    public function upload()
    {
        if ($this->validate() && isset($this->imageFile)) {
            $this->imageFile->saveAs('themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = 'themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    public function getBonusId($product_id)
    {
        $bonuses = ProductBonus::find()
            ->select('bonus_id')
            ->where(['product_id' => $product_id])
            ->column();
        return $bonuses;
    }

    public function getBonus($id)
    {
        return Bonus::find()
            ->where(['id' => $id])
            ->one();
    }

    public static function deleteItem($id)
    {
        $bonus = Bonus::findOne(['id' => $id]);
        return ($bonus->delete()) ? true : false;
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['rest/bonuses/' . $this->id], true),
            Link::REL_LIST => Url::to(['rest/bonuses/'], true),
        ];
    }
}