<?php

namespace app\controllers;

use app\models\Order;
use app\models\Product;
use yii\web\Controller;
use Yii;

class ProductController extends Controller
{
    public function actionIndex()
    {
        $products = new Product();
        $products = $products->find()->orderBy('id')
            ->all();

        $order = new Order();
        if ($order->load(Yii::$app->request->post()) && $order->validate()) {
            $order->total = Product::getPrice($order->product_id);
            $order->save(false);
            return $this->refresh();
        }

        return $this->render('index', compact('order', 'products'));
    }
}