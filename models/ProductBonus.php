<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductBonus extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{product_bonus}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['product_id', 'bonus_id'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'product_id' => 'Связанный продукт',
            'bonus_id' => 'Связанный бонус',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getBonus()
    {
        return $this->hasOne(Product::className(), ['id' => 'bonus_id']);
    }
}