<?php

namespace app\controllers\cms;

use yii\web\UploadedFile;
use app\models\Cake;
use app\models\ProductCake;
use app\models\CakeImage;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;

class CakeController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'cake', 'delete-cake'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['readCake'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cake'],
                        'roles' => ['updateCake'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete-cake'],
                        'roles' => ['deleteCake'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionIndex()
    {
        $cakes = Cake::find()
            ->orderBy('id')
            ->all();
        return $this->render('index', compact('cakes'));
    }

    public function actionCake($id = null)
    {

        $newImage = new CakeImage();
        if (isset($id)) {
            $cake = Cake::find()
                ->joinWith('cakeImages')
                ->where(['cake.id' => $id])
                ->one();
        } else {
            $cake = new Cake();
        }

        if (Yii::$app->request->isPost) {

            if ($cake->load(Yii::$app->request->post()) && $cake->validate()) {
                $cake->save(false);
                Yii::$app->session->setFlash('cakeadd');
            }

            $newImage->imageFile = UploadedFile::getInstance($newImage, 'imageFile');

            if ($newImage->upload()) {
                Yii::$app->session->setFlash('cakeadd');
                $newImage->cake_id = $cake->id;
                $newImage->save(false);
                $newImage->imageFile = '';
            }
        }
        //redirect for new item
        if ((Url::to() == '/landing/web/cms/cakes/cake') && $cake->id) {
            return $this->redirect(Url::to(['cms/cake/cake', 'id' => $cake->id]));
        }
        return $this->render('cake', compact('cake', 'newImage'));
    }

    public function actionDeleteImage($id, $cake_id)
    {
        $cakeImage = CakeImage::findOne([
            'id' => $id,
            'cake_id' => $cake_id
        ]);
        $cakeImage->delete();
        return $this->redirect('cake/' . $cake_id);
    }

    public function actionDeleteCake($id)
    {
        (Cake::deleteItem($id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/cakes');
    }
}