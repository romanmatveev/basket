<?php

use yii\db\Migration;

class m170831_062435_add_foreign_keys_to_product_cake extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-product-product_id',
            'product_cake',
            'product_id',
            'product',
            'id'
        );

        $this->addForeignKey(
            'fk-cake-cake_id',
            'product_cake',
            'cake_id',
            'cake',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-product-product_id',
            'product_cake'
        );

        $this->dropForeignKey(
            'fk-cake-cake_id',
            'product_cake'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_062435_add_foreign_keys_to_product_cake cannot be reverted.\n";

        return false;
    }
    */
}
