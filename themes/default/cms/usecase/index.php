<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Штуки';

?>
<div class="container">
    <h4>Список штук</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <?php if (Yii::$app->session->hasFlash('deleted')): ?>
                    <p class="alert-danger">Объект удален.</p>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
                    <p class="alert-danger">Ошибка при удалении!</p>
                <?php endif; ?>
                <table class="table">
                    <tbody>
                    <?php foreach ($usecases as $case) { ?>
                        <tr>
                            <td>
                                <?php if (\Yii::$app->user->can('updateUsecase')) { ?>
                                <?= Html::a(Html::encode($case->name), Url::to(['/cms/usecase/view', 'id' => $case->id])) ?>
                                <?php } else {
                                    echo Html::encode($case->name);
                                } ?>
                            </td>
                            <?php if (\Yii::$app->user->can('updateUsecase')) { ?>
                            <td>
                                <?= Html::a('Изменить', Url::to(['/cms/usecase/view', 'id' => $case->id]), ['class' => 'btn btn-primary']) ?>
                            </td>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('deleteUsecase')) { ?>
                            <td>
                                <?= Html::a('Удалить', Url::to(['/cms/usecase/delete', 'id' => $case->id]), ['class' => 'btn btn-danger']) ?>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <?php if (\Yii::$app->user->can('createUsecase')) { ?>
                                <?= Html::a('Новый usecase', Url::to(['/cms/usecase/view']), ['class' => 'btn btn-success']) ?>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>