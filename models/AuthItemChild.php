<?php

namespace app\models;


use yii\db\ActiveRecord;

class AuthItemChild extends ActiveRecord
{
    public static function tableName()
    {
        return '{{auth_item_child}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'parent' => 'Родитель',
            'child' => 'Ребенок',
        ];
    }

    public function getChild()
    {
        return $this->hasOne(AuthItemChild::className(), ['name' => 'child']);
    }

    public function getParent()
    {
        return $this->hasOne(AuthItemChild::className(), ['name' => 'parent']);
    }

}