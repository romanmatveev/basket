<?php

/* @var $this yii\web\View */

$this->title = 'Корзинка сладостей';

?>

<header class="header">
	<div class="h-menu">
		<div class="h-menu__container">

			<div class="h-menu__logo-box">
				<div class="h-menu-toggle">
					<button class="h-menu-toggle__button"></button>
				</div>
				<div class="h-logo">
					<a href="#" class="h-logo__link">
						<img class="h-logo__image" src="themes/default/img/logo-ks-150x56.svg" alt="Корзинка Сладостей">
					</a>
				</div>
			</div>
			<div class="h-menu__main">
				<div class="h-menu__main-content">
					<nav class="nav">
						<ul class="nav__list">
							<li class="nav__item">
								<a href="#products" class="nav__button">Наборы</a>
							</li>
							<li class="nav__item">
								<a href="#usecase" class="nav__button">Сценарии</a>
							</li>
							<li class="nav__item">
								<a href="#benefits" class="nav__button">Преимущества</a>
							</li>
							<li class="nav__item">
								<a href="#present" class="nav__button">Подарок</a>
							</li>
							<li class="nav__item">
								<a href="#gallery" class="nav__button">Галерея</a>
							</li>
							<li class="nav__item">
								<a href="#contacts" class="nav__button">Контакты</a>
							</li>
						</ul>
					</nav>
					<ul class="h-address">
						<li class="h-address__item h-address__item--phone">+7 (343) 290-12-90</li>
						<li class="h-address__item h-address__item--city">
							г. <span class="h-address__city">Eкатеринбург</span>
						</li>
					</ul>
				</div>
				<div class="h-menu__main-overlay"></div>
			</div>
		</div>
	</div>
</header>

<div class="hero">
	<div class="hero__content">
		<div class="hero__text">
			<span class="hero__ttl hero__ttl--1">Наборы сладостей</span>
			<span class="hero__ttl hero__ttl--2">С доставкой в офис и домой</span>
			<a href="#" class="button button--l button--white">Заказать</a>
		</div>
	</div>
	<div class="hero__media">
		<div class="hero-parallax">
			<div class="hero-parallax__layer-container hero-parallax__layer-container--4">
				<div class="hero-parallax__layer hero-parallax__layer--4 layer" data-depth="0.05"></div>
			</div>
			<div class="hero-parallax__layer-container hero-parallax__layer-container--3">
				<div class="hero-parallax__layer hero-parallax__layer--3 layer" data-depth="0.1"></div>
			</div>
			<div class="hero-parallax__layer-container hero-parallax__layer-container--2">
				<div class="hero-parallax__layer hero-parallax__layer--2 layer" data-depth="0.2"></div>
			</div>
			<div class="hero-parallax__layer-container hero-parallax__layer-container--1">
				<div class="hero-parallax__layer hero-parallax__layer--1 layer" data-depth="0.6"></div>
			</div>
		</div>
	</div>
</div>
<div class="p-card">
	<a name="products" class="anchor"></a>
	<div class="p-card__container">
		<button class="p-card__slider-prev"></button>
		<button class="p-card__slider-next"></button>
		<div class="p-card__slider">
			<div class="p-card__item">
				<div class="p-card__main">
					<div class="p-card__main-image">
						<img src="themes/default/img/img001.jpg" alt="">
					</div>
					<div class="p-card__content">
						<span class="p-card__ttl">Корзинка «Праздничная первая»</span>
						<div class="p-card__ctr p-card__ctr--mobile">
							<span class="p-card__ctr-current"><span class="p-card__ctr-num">1</span> набор</span>
							<span class="p-card__ctr-total">из <span class="p-card__ctr-num">7</span></span>
						</div>
						<div class="p-card__content-image">
							<img src="themes/default/img/img001.jpg" alt="">
						</div>
						<ul class="p-card__descr">
							<li class="p-card__descr-item">
								<span class="p-card__descr-name">Вес:</span>
								<span class="p-card__descr-value">2,5 кг</span>
							</li>
							<li class="p-card__descr-item">
								<span class="p-card__descr-name">Цена:</span>
								<span class="p-card__descr-value">1 600 &#8381;</span>
							</li>
						</ul>
						<hr class="p-card__hr">

						<div class="p-bonus">
							<div class="p-bonus__info">
								При заказе корзинки бонус на выбор:
							</div>
							<ul class="p-bonus__list">
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus" id="bonus-1">
									<label class="p-bonus__label" for="bonus-1">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Листовой чай</span>
										</span>
									</label>
								</li>
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus" id="bonus-2">
									<label class="p-bonus__label" for="bonus-2">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002b.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Кофе в зернах</span>
										</span>
									</label>
								</li>
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus" id="bonus-3">
									<label class="p-bonus__label" for="bonus-3">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002c.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Конфеты</span>
										</span>
									</label>
								</li>
							</ul>
						</div>
						<div class="p-card__buy-box">
							<a href="#" class="button button--l button--gradient p-card__buy p-card__buy--desktop">
								<span class="button__text">Заказать</span>
							</a>
							<div class="p-card__ctr p-card__ctr--desktop">
								<span class="p-card__ctr-current"><span class="p-card__ctr-num">1</span> набор</span>
								<span class="p-card__ctr-total">из <span class="p-card__ctr-num">7</span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="p-card__item">
				<div class="p-card__main">
					<div class="p-card__main-image">
						<img src="themes/default/img/korzinka-evropeyskaya-7.jpg" alt="">
					</div>
					<div class="p-card__content">
						<span class="p-card__ttl">Корзинка «Европейская 7»</span>
						<div class="p-card__ctr p-card__ctr--mobile">
							<span class="p-card__ctr-current"><span class="p-card__ctr-num">1</span> набор</span>
							<span class="p-card__ctr-total">из <span class="p-card__ctr-num">7</span></span>
						</div>
						<div class="p-card__content-image">
							<img src="themes/default/img/korzinka-evropeyskaya-7.jpg" alt="">
						</div>
						<ul class="p-card__descr">
							<li class="p-card__descr-item">
								<span class="p-card__descr-name">Вес:</span>
								<span class="p-card__descr-value">2,5 кг</span>
							</li>
							<li class="p-card__descr-item">
								<span class="p-card__descr-name">Цена:</span>
								<span class="p-card__descr-value">1 600 &#8381;</span>
							</li>
						</ul>
						<hr class="p-card__hr">

						<div class="p-bonus">
							<div class="p-bonus__info">
								При заказе корзинки бонус на выбор:
							</div>
							<ul class="p-bonus__list">
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus2" id="bonus2-1">
									<label class="p-bonus__label" for="bonus2-1">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Листовой чай</span>
										</span>
									</label>
								</li>
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus2" id="bonus2-2">
									<label class="p-bonus__label" for="bonus2-2">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002b.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Кофе в зернах</span>
										</span>
									</label>
								</li>
								<li class="p-bonus__item">
									<input type="radio" class="p-bonus__radio" name="bonus2" id="bonus2-3">
									<label class="p-bonus__label" for="bonus2-3">
										<span class="p-bonus__image" style="background-image: url('themes/default/img/img002c.jpg');"></span>
										<span class="p-bonus__text">
											<span class="p-bonus__ico"></span>
											<span class="p-bonus__name">Конфеты</span>
										</span>
									</label>
								</li>
							</ul>
						</div>
						<div class="p-card__buy-box">
							<a href="#" class="button button--l button--gradient p-card__buy p-card__buy--desktop">
								<span class="button__text">Заказать</span>
							</a>
							<div class="p-card__ctr p-card__ctr--desktop">
								<span class="p-card__ctr-current"><span class="p-card__ctr-num">1</span> набор</span>
								<span class="p-card__ctr-total">из <span class="p-card__ctr-num">7</span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="p-card__components">
			<div class="p-card__components-slider">
				<div class="p-one">
					<div class="p-one__slider-container">
						<button class="p-one__slider-prev"></button>
						<button class="p-one__slider-next"></button>
						<div class="p-one__slider">
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-1.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-1.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-1b.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-1b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Пирожное «Прага»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>

							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-1.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-1.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-1b.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-1b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Пирожное «Прага»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>

							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-pervaya-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-pervaya-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="button button--l button--gradient p-card__buy p-card__buy--mobile">
						<span class="button__text">Заказать</span>
					</a>
				</div>
				<div class="p-one">
					<div class="p-one__slider-container">
						<button class="p-one__slider-prev"></button>
						<button class="p-one__slider-next"></button>
						<div class="p-one__slider">
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-1.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-1.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-1b.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-1b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Пирожное «Прага»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-1.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-1.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-1b.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-1b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Пирожное «Прага»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
							<div class="p-one__item">
								<div class="p-one__item-container">
									<div class="p-one__image-container">
										<span class="p-one__total">10</span>
										<div class="p-one__image-list">
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2.jpg);"></a>
											<a class="p-one__image" href="themes/default/img/korzinka-evropeyskaya-one-2b.jpg"
												style="background-image: url(themes/default/img/korzinka-evropeyskaya-one-2b.jpg);"></a>
										</div>
									</div>
									<div class="p-one__text">
										<span class="p-one__ttl">Профитроли «Сливочные»</span>
										<a href="#" class="p-one__ingredients">Посмотреть состав</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a href="#" class="button button--l button--gradient p-card__buy p-card__buy--mobile">
						<span class="button__text">Заказать</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="usecase">
	<a name="usecase" class="anchor"></a>
	<div class="usecase__container">
		<button class="usecase__slider-prev"></button>
		<button class="usecase__slider-next"></button>
		<div class="usecase__slider-wrapper">
			<div class="usecase__slider">
				<div class="usecase__item">
					<div class="usecase__image-container">
						<div class="usecase__image" style="background-image: url(themes/default/img/usecase2.jpg);"></div>
					</div>
					<div class="usecase__text">Порадовать родных и друзей в праздник<br>или просто так</div>
				</div>
				<div class="usecase__item">
					<div class="usecase__image-container">
						<div class="usecase__image" style="background-image: url(themes/default/img/usecase1.jpg);"></div>
					</div>
					<div class="usecase__text">Порадовать родных и друзей в праздник<br>или просто так</div>
				</div>
				<div class="usecase__item">
					<div class="usecase__image-container">
						<div class="usecase__image" style="background-image: url(themes/default/img/img004.png);"></div>
					</div>
					<div class="usecase__text">Порадовать родных и друзей в праздник<br>или просто так</div>
				</div>
				<div class="usecase__item">
					<div class="usecase__image-container">
						<div class="usecase__image" style="background-image: url(themes/default/img/usecase1.jpg);"></div>
					</div>
					<div class="usecase__text">Порадовать родных и друзей в праздник<br>или просто так</div>
				</div>
			</div>
		</div>
	</div>
	<div class="usecase__slider-dots"></div>
</div>
<div class="benefits">
	<a name="benefits" class="anchor"></a>
	<div class="benefits__container">
		<ul class="benefits__list">
			<li class="benefits__item">
				<div class="benefits__image"
					style="background-image: url(themes/default/img/ico-230x200-benefits-product.png);"></div>
				<span class="benefits__text">Сделано из свежих и натуральных продуктов</span>
			</li>
			<li class="benefits__item">
				<div class="benefits__image"
					style="background-image: url(themes/default/img/ico-230x200-benefits-box.png);"></div>
				<span class="benefits__text">Все расфасовано в жесткие коробки и не мнется</span>
			</li>
			<li class="benefits__item">
				<div class="benefits__image"
					style="background-image: url(themes/default/img/ico-230x200-benefits-kit.png);"></div>
				<span class="benefits__text">В наборе есть тарелки, салфетки и перчатки для удобства</span>
			</li>
		</ul>
	</div>
</div>
<div class="present">
	<a name="present" class="anchor"></a>
	<div class="present__container">
		<div class="present__image"></div>
		<div class="present__content">
			<span class="present__ttl">Подарите радость</span>
			<p>Мы доставляем корзинки сладостей в качестве подарков. Набор упакован в бумагу, обвязан атласной лентой и вложена открытка с пожеланием. Готовый подарок мы доставим по указанному вами адресу в нужную дату и время.</p>
			<p>&nbsp;</p>
			<div class="present__price">
				<span class="present__price-name">Стоимость услуги:</span>
				<span class="present__price-value">300&#8381;</span>
			</div>
			<a href="#" class="button button--l button--gradient"><span class="button__text">Заказать</span></a>
		</div>
	</div>
</div>
<div class="p-set">
	<a name="gallery" class="anchor"></a>
	<div class="p-set__container">
		<div class="p-set__list">
			<div class="p-set__item">
				<div class="p-set__main">
					<a class="p-set__image" href="themes/default/img/gallery1.jpg" style="background-image: url(themes/default/img/gallery1.jpg);"></a>
				</div>
			</div>
			<div class="p-set__item">
				<div class="p-set__main">
					<a class="p-set__image" href="themes/default/img/gallery1.jpg" style="background-image: url(themes/default/img/gallery1.jpg);"></a>
				</div>
			</div>
			<div class="p-set__item">
				<div class="p-set__main">
					<a class="p-set__image" href="themes/default/img/gallery1.jpg" style="background-image: url(themes/default/img/gallery1.jpg);"></a>
					<a href="#" class="p-set__content">
						<div class="p-set__content-wrapper">
							<span class="p-set__title">
								<span class="p-set__title-small">Подарочный набор</span>
								<span class="p-set__title-big">«Лучшие сладости Европы»</span>
							</span>
							<span class="button button--l button--white p-set__buy">Заказать</span>
						</div>
					</a>
				</div>
			</div>
			<div class="p-set__item">
				<div class="p-set__main">
					<a class="p-set__image" href="themes/default/img/gallery1.jpg" style="background-image: url(themes/default/img/gallery1.jpg);"></a>
					<a href="#" class="p-set__content">
						<div class="p-set__content-wrapper">
							<span class="p-set__title">
								<span class="p-set__title-small">Подарочный набор</span>
								<span class="p-set__title-big">«Лучшие сладости Европы»</span>
							</span>
							<span class="button button--l button--white p-set__buy">Заказать</span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="contacts">
	<a name="contacts" class="anchor"></a>
	<div class="contacts__map">
		<div class="contacts__ymap" id="map"></div>
	</div>
	<div class="contacts__main">
		<div class="contacts__content">
			<ul class="tab__nav tab__nav--contacts">
				<li class="tab__nav-item tab__nav-item--active">Контакты</li>
				<li class="tab__nav-item">Доставка</li>
			</ul>
			<div class="tab__content-container tab__content-container--contacts">
				<div class="tab__content tab__content--active">
					<ul class="contacts__list">
						<li>Диспетчер: +7 (343) 290-12-90</li>
						<li>Менеджер: +7 (982) 710-10-70</li>
						<li>E-mail: sweetbasket@list.ru</li>
					</ul>
				</div>
				<div class="tab__content">
					<p>Доставляем бесплатно! С 8 до 22 ежедневно.</p>
					<p>Курьер привезет вашу корзинку в любой район Екатеринбурга в течение 1 часа. В Верхнюю Пышму, Арамиль, Березовский и Среднеуральск – в течение 3 часов.</p>
				</div>
			</div>
			<form class="contacts__form form--white">
				<div class="field__item">
					<input type="text" class="field__elem" placeholder="ФИО">
				</div>
				<div class="field__item">
					<input type="text" class="field__elem" placeholder="Номер телефона">
				</div>
				<div class="field__item">
					<input type="text" class="field__elem" placeholder="Адрес">
				</div>
				<div class="field__item">
					<input type="text" class="field__elem" placeholder="E-mail">
				</div>
				<div class="field__item">
					<textarea class="field__elem" placeholder="Комментарии"></textarea>
				</div>
				<a href="#" class="button button--l button--gradient contacts__buy">
					<span class="button__text">Заказать</span>
				</a>
			</form>
		</div>
	</div>
</div>

<footer class="footer">
	<button class="scrollup"></button>
	<div class="footer__container">
		<div class="footer__left">
			<span>2015 © ООО "Корзинка сладостей" Доставка кондитерских изделий</span>
		</div>
		<div class="footer__right">
			<ul class="f-soc">
				<li class="f-soc__item f-soc__item--f">
					<a href="#" class="f-soc__button"></a>
				</li>
				<li class="f-soc__item f-soc__item--v">
					<a href="#" class="f-soc__button"></a>
				</li>
				<li class="f-soc__item f-soc__item--t">
					<a href="#" class="f-soc__button"></a>
				</li>
				<li class="f-soc__item f-soc__item--i">
					<a href="#" class="f-soc__button"></a>
				</li>
			</ul>
		</div>
	</div>
</footer>
<script src="js/main.min.js"></script>
