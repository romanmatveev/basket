<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корзинки';

?>
<div class="container">
    <h4>Список товаров</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <?php if (Yii::$app->session->hasFlash('deleted')): ?>
                    <p class="alert-danger">Объект удален.</p>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
                    <p class="alert-danger">Ошибка при удалении!</p>
                <?php endif; ?>
                <table class="table">
                    <tbody>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td>
                                <?php if (\Yii::$app->user->can('updateProduct')) { ?>
                                    <?= Html::a(Html::encode($product->name), Url::to(['/cms/backend/product', 'id' => $product->id])) ?>
                                <?php } else {
                                    echo Html::encode($product->name);
                                } ?>
                            </td>
                            <?php if (\Yii::$app->user->can('updateProduct')) { ?>
                            <td>
                                <?= Html::a('Изменить', Url::to(['/cms/backend/product', 'id' => $product->id]), ['class' => 'btn btn-primary']) ?>
                            </td>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('deleteProduct')) { ?>
                            <td>
                                <?= Html::a('Удалить', Url::to(['/cms/backend/delete-product', 'id' => $product->id]), ['class' => 'btn btn-danger']) ?>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <?php if (\Yii::$app->user->can('createProduct')) { ?>
                    <tr>
                        <td>
                            <?= Html::a('Новый товар', Url::to(['/cms/backend/product']), ['class' => 'btn btn-success']) ?>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>