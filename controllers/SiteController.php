<?php

namespace app\controllers;

use app\models\Content;
use app\models\Order;
use app\models\Product;
use app\models\Usecase;
use himiklab\ipgeobase\IpGeoBase;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $order = new Order();
        if ($order->load(Yii::$app->request->post()) && $order->validate()) {
            $order->save(false);
            return $this->refresh();
        }
        $usecases = Usecase::find()
            ->orderBy('id')
            ->all();
        
        $content = new Content();
        $content = $content->getContent();
        $products = new Product();
        $products = $products->find()->orderBy('id')
            ->all();
        return $this->render('index', compact('order', 'content', 'products', 'usecases'));
    }
    
    /**
     * Login action.
     *
     * @return Response|string
     */
//    public function actionLogin()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//            'model' => $model
//        ]);
//    }

    /**
     * Logout action.
     *
     * @return Response
     */
    /*public function actionLogout()
    {
        Yii::$app->user->logout();

        $model = new LoginForm();

        return $this->render('login', [
            'model' => $model
        ]);
    }*/

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/

    /**
     * Displays about page.
     *
     * @return string
     */
    /*public function actionAbout()
    {
        return $this->render('about');
    }*/
}
