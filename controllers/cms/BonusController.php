<?php

namespace app\controllers\cms;

use app\models\ProductBonus;
use yii\web\UploadedFile;
use app\models\Bonus;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;

class BonusController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['readBonus'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['updateBonus'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteBonus'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionIndex()
    {
        $bonuses = Bonus::find()
            ->orderBy('id')
            ->all();
        return $this->render('index', compact('bonuses'));
    }

    public function actionView($id = null)
    {
        if (isset($id)) {
            $bonus = Bonus::find()
                ->where(['id' => $id])
                ->one();
        } else {
            $bonus = new Bonus();
        }

        if (Yii::$app->request->isPost) {

            if ($bonus->load(Yii::$app->request->post()) && $bonus->validate()) {
                $bonus->save(false);
            }

            $bonus->imageFile = UploadedFile::getInstance($bonus, 'imageFile');

            if ($bonus->upload()) {
                Yii::$app->session->setFlash('cakeadd');
                $bonus->save(false);
                $bonus->imageFile = '';
            }
        }
        //redirect for new item
        if ((Url::to() == '/landing/web/cms/bonuses/view') && $bonus->id) {
            return $this->redirect(Url::to(['cms/bonus/view', 'id' => $bonus->id]));
        }
        return $this->render('view', compact('bonus'));
    }

    public function actionDelete($id)
    {
        (Bonus::deleteItem($id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/bonuses');
    }
}