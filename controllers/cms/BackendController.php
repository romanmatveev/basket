<?php

namespace app\controllers\cms;

use app\models\Content;
use app\models\LoginForm;
use app\models\Product;
use app\models\Cake;
use app\models\ProductCake;
use app\models\Bonus;
use app\models\ProductBonus;
use yii\base\Model;
use yii\helpers\Url;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class BackendController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'only' => ['login', 'logout', 'index', 'product', 'products', 'delete-product'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['readContent'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['products'],
                        'roles' => ['readProduct'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['product'],
                        'roles' => ['updateProduct'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete-product'],
                        'roles' => ['deleteProduct'],

                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()) {
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionIndex()
    {
        $content = new Content();
        $content = $content->getContent(1);
        if ($content->load(Yii::$app->request->post()) && $content->validate()) {
            Yii::$app->session->setFlash('changessaved');
            $content->save(false);
        }
        return $this->render('index', compact('content'));
    }

    public function actionLogin()
    {

        $user = new LoginForm();
        if ($user->load(Yii::$app->request->post()) && $user->login()) {
            return $this->redirect('@web/cms');
        }
        return $this->render('login', ['user' => $user]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('login');
    }

    public function actionProducts()
    {
        $products = Product::find()
            ->orderBy('id')
            ->all();
        return $this->render('products', compact('products'));
    }

    public function actionProduct($id = null)
    {
        $cakeItem = new Cake();
        $newCake = new ProductCake();
        $newBonus = new Bonus();
        $productBonus = new ProductBonus();
        $items = Cake::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->column();
        $bonuses = Bonus::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->column();

        if (isset($id)) {
            $product = Product::find()
                ->where(['id' => $id])
                ->one();
        } else {
            $product = new Product();
        }

        if (Yii::$app->request->isPost) {

            if (Model::loadMultiple($product->productCake, Yii::$app->request->post()) && Model::validateMultiple($product->productCake)) {
                foreach ($product->productCake as $quantity) {
                    if ($quantity->save(false)) {
                        Yii::$app->session->setFlash('cakeadd');
                    }
                }
            }

            //new image
            if ($product->load(Yii::$app->request->post()) && $product->validate()) {
                $product->imageFile = UploadedFile::getInstance($product, 'imageFile');
                if ($product->upload()) {
                }
                $product->save(false);
            }

            //bonus
            if ($newBonus->load(Yii::$app->request->post())) {
                $productBonus->product_id = $product->id;
                $productBonus->bonus_id = $newBonus->name;
                $productBonus->save(false);
            }

            //new cake
            if ($cakeItem->load(Yii::$app->request->post()) && $newCake->load(Yii::$app->request->post())) {
                $this->actionAddCake($product->id, $cakeItem->name, $newCake->quantity);
            }

        }
        //redirect for new item
        if ((Url::to() == '/landing/web/cms/products/product') && $product->id) {
            return $this->redirect(Url::to(['cms/backend/product', 'id' => $product->id]));
        }

        return $this->render('product', compact('product', 'items', 'newCake', 'cakeItem', 'bonuses', 'newBonus'));
    }

    public function actionAddCake($product_id, $cake, $quantity)
    {
        if (!$new = ProductCake::find([
                'product_id' => $product_id,
                'cake_id' => $cake
            ])) {
            $new = new ProductCake();
            $new->product_id = $product_id;
            $new->cake_id = $cake;
            $new->quantity = $quantity;
            $new->save();
        } else {
            Yii::$app->session->setFlash('cakeused');
        }
        return $this->redirect(Url::to(['/cms/backend/product', 'id' => $product_id]));

    }

    public function actionDeleteCake($product_id, $cake_id)
    {
        (Product::deleteCake($product_id, $cake_id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect(Url::to(['/cms/backend/product', 'id' => $product_id]));
    }

    public function actionDeleteBonus($product_id, $bonus_id)
    {
        (Product::deleteBonus($product_id, $bonus_id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect(Url::to(['/cms/backend/product', 'id' => $product_id]));
    }

    public function actionDeleteProduct($id)
    {
        (Product::deleteItem($id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/products');
    }

    public function actionAccessDeny()
    {
        return $this->render('accessDeny');
    }
}