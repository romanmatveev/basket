<?php

namespace app\controllers;

use app\models\Order;
use app\models\Product;
use app\models\Content;
use yii\web\Controller;
use Yii;

class GiftsetController extends Controller
{
    public function actionIndex()
    {
        $products = new Product();
        $products = $products->find()
            ->orderBy('id')
            ->all();
        
        $content = new Content();
        $content = $content->getContent();

        $order = new Order();
        if ($order->load(Yii::$app->request->post()) && $order->validate()) {
            if ($order->product_id) {
                $order->total = Product::getPrice($order->product_id) + $content->present_price;
            }
            $order->present = 1;
            $order->save(false);
            return $this->refresh();
        }

        return $this->render('index', compact('order', 'products', 'content'));
    }
}