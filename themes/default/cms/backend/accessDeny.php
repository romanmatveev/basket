<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Доступ запрещен';

?>
<div class="container">
    <h3>Доступ запрещен.</h3>
    <div class="row">
        <div class="col-md-12 blog-main">
            <div class="blog-post">
                <div class="blog-post">

                    <p>Пожалуйста, обратитесь к вашему администратору.</p>
                    <?= Html::a('Домой', Url::toRoute(["/cms"]), ['class' => 'btn btn-info']) ?>
                </div>
            </div>
        </div>
    </div>
</div>