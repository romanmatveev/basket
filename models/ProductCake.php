<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductCake extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{product_cake}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['product_id', 'cake_id', 'quantity'], 'required'],
            ['quantity', 'integer', 'min' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'product_id' => 'Связанный продукт',
            'cake_id' => 'Связанный ингридиент',
            'quantity' => 'Кол-во',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    public function getCake()
    {
        return $this->hasOne(Product::className(), ['id' => 'cake_id']);
    }

    public static function getQuanity($id, $cake_id)
    {
        $quantity = ProductCake::find()
            ->where ([
                'product_id' => $id,
                'cake_id' => $cake_id,
            ])
            ->one();
        return $quantity->quantity;
    }
}