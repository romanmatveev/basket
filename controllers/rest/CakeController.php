<?php

namespace app\controllers\rest;

use yii\filters\AccessControl;
use yii\rest\ActiveController;
use app\models\Cake;
use app\models\CakeImage;
use Prophecy\Exception\Doubler\DoubleException;
use yii\filters\auth\HttpBasicAuth;
use yii\base\InvalidValueException;
use app\models\User;
use yii;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class CakeController extends ActiveController
{
    public $modelClass = 'app\models\Cake';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if ($user->validatePassword($password)) {
                    return $user;
                }
            },
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['apiPermission'],
                ],
            ],
            'denyCallback' => function () {
                throw new yii\web\NotAcceptableHttpException('Not enough permissions for this operations');
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new Cake();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/cakes/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = Cake::findOne(['id' => $id]);
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(202);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/cakes/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionDelete($id)
    {
        if (Cake::deleteItem($id)) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(204);
            $response->getHeaders()->set('Location', Url::to('@web/rest/cakes'));
            return $response;
        } else {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }

    public function actionAddImage()
    {
        $model = new CakeImage();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $filename =  Yii::getAlias('@web') . "themes/default/img/" . basename($model->image);
        if (!CakeImage::findOne(['cake_id' => $model->cake_id, 'image' => $filename])) {

            if ($model->image) {
                $model->image = CakeImage::uploadFile($model->image);
                
                if ($model->save()) {
                    $response = Yii::$app->getResponse();
                    $response->setStatusCode(202);
                    $response->getHeaders()->set('Location', Url::to('@web/rest/cakes/'));
                } elseif (!$model->hasErrors()) {
                    throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
                }
            }
        } else {
            throw new DoubleException;
        }
        $model = Cake::findOne(['id' => $model->cake_id]);
        return $model;
    }

    public function actionDeleteImage()
    {
        $image_id = Yii::$app->getRequest()->getBodyParam('id');
        $cake_id = Yii::$app->getRequest()->getBodyParam('cake_id');
        if (CakeImage::deleteAll(['id' => $image_id, 'cake_id' => $cake_id])) {
            return Cake::findOne(['id' => $cake_id]);
        } else {
            throw new InvalidValueException;
        }
    }
}