<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use app\models\ProductCake;

$this->title = 'Product editor';
?>

<div class="container">
    <h4>Страница редактирования товара</h4>
    <div class="row">
        <div class="col-md-12 blog-main">
            <div class="blog-post">
                <div class="blog-post">
                    <?php if (Yii::$app->session->hasFlash('deleted')): ?>
                        <p class="alert-danger">Объект удален.</p>
                    <?php endif; ?>
                    <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
                        <p class="alert-danger">Ошибка при удалении!</p>
                    <?php endif; ?>
                    <?php if (Yii::$app->session->hasFlash('cakeadd')): ?>
                        <p class="alert-danger">Изменения сохранены.</p>
                    <?php endif; ?>
                    <?php if (Yii::$app->session->hasFlash('cakeused')): ?>
                        <p class="alert-danger">Пирожное уже есть в корзинке.</p>
                    <?php endif; ?>
                    <?php if (Yii::$app->session->hasFlash('cakenotsaved')): ?>
                        <p class="alert-danger">Пирожное не сохранено.</p>
                    <?php endif; ?>
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'text1-form',
                    ]) ?>
                    <div class="col-md-12">
                        <?= $form->field($product, 'name')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($product, 'weight')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($product, 'price')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?php if ($product->image) { ?>
                            <img src="<?= 'http://localhost/landing/web/' . $product->image; ?>" width="250px">
                        <?php } ?>
                        <?= $form->field($product, 'imageFile')->fileInput()->label(false); ?>
                    </div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td><h5>Пирожные:</h5></td>
                        </tr>

                        <?php foreach ($product->cakes as $cake) { ?>
                            <tr>
                                <td width="20%">
                                    <div class="col-md-11">
                                        <?= $cake->name ?>
                                    </div>
                                </td>
                                <?php foreach ($product->productCake as $index => $quantity) { ?>
                                    <?php if ($quantity->cake_id == $cake->id) { ?>
                                        <td width="10%"><?= $form->field($quantity, "[$index]quantity")->textInput(['class' => 'col-md-11'])->label(false) ?></td>
                                        </td>
                                    <?php } ?>
                                <?php } ?>

                                <td width="10%">
                                    <?= Html::a('Удалить', Url::to(['/cms/products/delete-cake', 'product_id' => $product->id, 'cake_id' => $cake->id,]), ['class' => 'btn btn-danger col-md-11']) ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if (isset($_POST['addCake'])) { ?>
                            <tr id="newCake">
                                <td width="20%">
                                    <div class="col-md-11">
                                        <?= $form->field($cakeItem, 'name')->dropdownList($items,
                                            [
                                                'prompt' => 'Выбери пироженку',
                                                'class' => 'col-md-12'
                                            ])->label(false) ?>
                                    </div>
                                </td>
                                <td width="10%">
                                    <?= $form->field($newCake, 'quantity')->textInput(['class' => 'col-md-11'])->label(false) ?>
                                </td>
                                <td width="10%">
                                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary col-md-11']) ?>
                                </td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td>
                                    <button formmethod="post" type="submit" name="addCake" class="btn btn-primary">
                                        Добавить пирожное
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td><h5>Бонусы:</h5></td>
                        </tr>
                        <?php foreach ($product->bonuses as $bonus) { ?>
                            <tr>
                                <td width="10%"><?= $bonus->name ?></td>
                                <td width="10%"><?= Html::a('Удалить', Url::to(['/cms/products/delete-bonus', 'product_id' => $product->id, 'bonus_id' => $bonus->id,]), ['class' => 'btn btn-danger col-md-11']) ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (isset($_POST['addBonus'])) { ?>
                            <tr>
                                <td width="20%">
                                    <div class="col-md-11">
                                        <?= $form->field($newBonus, 'name')->dropdownList($bonuses,
                                            [
                                                'prompt' => 'Выбери бонус',
                                                'class' => 'col-md-12'
                                            ])->label(false) ?>
                                    </div>
                                </td>
                                <td>
                                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary col-md-11']) ?>
                                </td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td>
                                    <button formmethod="post" type="submit" name="addBonus" class="btn btn-primary">
                                        Добавить бонус
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td width="5%">
                                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                                <?= Html::a('Назад', Url::toRoute(["/cms/products/"]), ['class' => 'btn btn-info']) ?>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<!--<p>--><? //= VarDumper::dump($product) ?><!--</p>-->

