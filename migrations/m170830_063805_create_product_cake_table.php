<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_cake`.
 */
class m170830_063805_create_product_cake_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_cake', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'cake_id' => $this->integer(),
            'quantity' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_cake');
    }
}
