<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;
use yii\filters\RateLimitInterface;

class User extends ActiveRecord implements IdentityInterface, Linkable, RateLimitInterface
{
    public static function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['username', 'string', 'max' => 50, 'min' => 4],
            ['password', 'string', 'min' => 4],
            [['allowance_updated_at', 'allowance'], 'trim']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function getRole()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public static function deleteItem($id)
    {
        AuthAssignment::deleteAll(['user_id' => $id]);

        $user = User::findOne(['id' => $id]);
        if ($user->delete()) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUserRole($id)
    {
        $role = AuthAssignment::findOne(['user_id' => $id]);
        if(!$role) {
            $role = new AuthAssignment();
        }
        return $role;
    }
    
    //REST
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['password'], $fields['authKey'], $fields['accessToken'], $fields['allowance'], $fields['allowance_updated_at']);

        return $fields;
    }

    public function extraFields()
    {
        return ['password'];
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['rest/users/' . $this->id], true),
            Link::REL_LIST => Url::to(['rest/users/'], true),
        ];
    }

    public function getRateLimit($request, $action)
    {
        return [100, 300];
    }

    public function loadAllowance($request, $action)
    {
        return [$this->allowance, $this->allowance_updated_at];
    }

    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        $this->allowance = $allowance;
        $this->allowance_updated_at = $timestamp;
        $this->save();
    }
}
