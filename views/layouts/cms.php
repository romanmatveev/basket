<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppCms;
use yii\helpers\Url;

AppCms::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="blog-masthead">
        <?php
        NavBar::begin([
            'options' => [
                'class' => 'nav',
            ],
        ]);
        echo Nav::widget([
            'items' => [
                [
                    'label' => 'Контент',
                    'url' => ['/cms/backend/index'],
                    'linkOptions'=>['class'=>'nav-link'],
                    'active' => ['class'=>'nav-link']
                ],
                [
                    'label' => 'Заказы',
                    'url' => ['/cms/orders'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                [
                    'label' => 'Товары',
                    'url' => ['/cms/products'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                [
                    'label' => 'Пирожные',
                    'url' => ['/cms/cakes'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                [
                    'label' => 'Бонусы',
                    'url' => ['/cms/bonuses'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                [
                    'label' => 'Штуки',
                    'url' => ['/cms/usecases'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                [
                    'label' => 'Пользователи',
                    'url' => ['/cms/users'],
                    'linkOptions'=>['class'=>'nav-link']
                ],
                Yii::$app->user->isGuest ? (
                [
                    'label' => 'Войти',
                    'url' => ['/cms/login'],
                    'linkOptions'=>['class'=>'nav-link']]
                ) : (
                [
                    'label' => 'Выйти (' . Html::encode(Yii::$app->user->identity->username) . ')',
                    'url' => ['/cms/logout'],
                    'linkOptions'=>['class'=>'nav-link']
                ]
                )
            ]
        ]);
        NavBar::end();
        ?>
    </div>
</div>

<?= $content ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
