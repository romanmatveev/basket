<?php

namespace app\controllers\rest;

use app\models\ProductCake;
use Prophecy\Exception\Doubler\DoubleException;
use app\models\Product;
use app\models\ProductBonus;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use app\models\User;
use yii\filters\AccessControl;
use yii;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\base\InvalidValueException;

class ProductController extends ActiveController
{
    public $modelClass = 'app\models\Product';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if ($user->validatePassword($password)) {
                    return $user;
                }
            },
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['apiPermission'],
                ],
            ],
            'denyCallback' => function () {
                throw new yii\web\NotAcceptableHttpException('Not enough permissions for this operations');
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new Product();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->image) {
            $putdata = fopen("file:///" . $model->image, "r");

            $short_path = "themes/default/img/" . basename($model->image);
            $path = \yii::getAlias('@webroot/') . $short_path;

            $fp = fopen($path, "w");

            while ($data = fread($putdata, 1024))
                fwrite($fp, $data);

            fclose($fp);
            fclose($putdata);

            $model->image = $short_path;
        }
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = Product::findOne(['id' => $id]);
        $old_image = $model->image;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->image != $old_image) {
            $putdata = fopen("file:///" . $model->image, "r");

            $short_path = "themes/default/img/" . basename($model->image);
            $path = \yii::getAlias('@webroot/') . $short_path;

            $fp = fopen($path, "w");

            while ($data = fread($putdata, 1024))
                fwrite($fp, $data);

            fclose($fp);
            fclose($putdata);

            $model->image = $short_path;
        }
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(202);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    public function actionDelete($id)
    {
        if (Product::deleteItem($id)) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(204);
            $response->getHeaders()->set('Location', Url::to('@web/rest/products'));
            return $response;
        } else {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }

    public function actionAddBonus()
    {
        $model = new ProductBonus();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if (!ProductBonus::findOne(['product_id' => $model->product_id, 'bonus_id' => $model->bonus_id])) {
            if ($model->save()) {
                $model = Product::findOne(['id' => $model->product_id]);
                $response = Yii::$app->getResponse();
                $response->setStatusCode(202);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
            } elseif (!$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        } else {
            throw new DoubleException;
        }
        return $model;
    }

    public function actionAddCake()
    {
        $model = new ProductCake();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if (!ProductBonus::findOne(['product_id' => $model->product_id, 'cake_id' => $model->cake_id])) {
            if ($model->save()) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(202);
                $model = Product::findOne(['id' => $model->product_id]);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
            } elseif (!$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        } else {
            throw new DoubleException;
        }

        return $model;
    }

    public function actionDeleteBonus()
    {
        $bonus_id = Yii::$app->getRequest()->getBodyParam('bonus_id');
        $product_id = Yii::$app->getRequest()->getBodyParam('product_id');
        if (ProductBonus::deleteAll(['bonus_id' => $bonus_id, 'product_id' => $product_id])) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(202);
            $model = Product::findOne(['id' => $product_id]);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
            return $model;
        } else {
            throw new InvalidValueException;
        }
    }

    public function actionDeleteCake()
    {
        $cake_id = Yii::$app->getRequest()->getBodyParam('cake_id');
        $product_id = Yii::$app->getRequest()->getBodyParam('product_id');
        if (ProductBonus::deleteAll(['cake_id' => $cake_id, 'product_id' => $product_id])) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(202);
            $model = Product::findOne(['id' => $product_id]);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/products/' . $id));
            return $model;
        } else {
            throw new InvalidValueException;
        }
    }
}