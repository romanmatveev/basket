<?php

use yii\db\Migration;

/**
 * Handles the creation of table `content`.
 */
class m170829_051821_create_content_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content', [
            'id' => $this->primaryKey(),
            'label' => $this->string(20)->unique(),
            'text' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content');
    }
}
