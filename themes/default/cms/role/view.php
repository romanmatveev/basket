<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Роль';

?>

<div class="container">
    <h4>Роль</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <div class="blog-post">
                    <?php if (Yii::$app->session->hasFlash('rolewaschanged')): ?>
                        <p class="alert-danger">Изменения сохранены.</p>
                    <?php endif; ?>
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                        'fieldConfig' => [
                            'template' => "{label}: {input}\n{hint}\n{error}",
                            ],
                    ]) ?>
                    <div class="col-md-12">
                        <?= $form->field($item, 'name')->textInput()->label() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($item, 'description')->textInput()->label() ?>
                    </div>
                    <h5>Разрешения:</h5>
                    <?php foreach ($permissions as $permission) { ?>
                        <?php if ((isset($rules)) && (in_array($permission, $rules))) { ?>
                            <div>
                                <input type="checkbox" name="checkboxes[]" id="<?= $permission ?>" value="<?= $permission ?>" checked>
                                <label for="<?= $permission ?>"><?= $permission ?></label></input>
                            </div>
                        <?php } else { ?>
                            <div>
                                <input type="checkbox" name="checkboxes[]" id="<?= $permission ?>" value="<?= $permission ?>">
                                <label for="<?= $permission ?>"><?= $permission ?></label></input>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                        &nbsp;&nbsp;
                        <?= Html::a('Back', Url::toRoute(["/cms/users/"]), ['class' => 'btn btn-info']) ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
