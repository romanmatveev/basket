<?php

namespace app\commands;

use app\models\AuthItem;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

//        $userRule = new UserRule;
//        $auth->add($userRule);

        $updateUser = AuthItem::find()
            ->where(['name' => 'updateUser'])
            ->one();

        $updateUser->rule_name = 'UserRule';
        $updateUser->save();
    }
}