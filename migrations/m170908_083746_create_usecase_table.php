<?php

use yii\db\Migration;

/**
 * Handles the creation of table `usecase`.
 */
class m170908_083746_create_usecase_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('usecase', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('usecase');
    }
}
