<?php

use yii\db\Migration;

class m170830_065419_add_column_to_content_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('content', 'text');
        $this->dropColumn('content', 'label');
        $this->addColumn('content', 'header', $this->string());
        $this->addColumn('content', 'descriptor', $this->string());
        $this->addColumn('content', 'present_title', $this->string());
        $this->addColumn('content', 'present_description', $this->string());
        $this->addColumn('content', 'present_price', $this->string());
        $this->addColumn('content', 'disp_phone', $this->string());
        $this->addColumn('content', 'manager_phone', $this->string());
        $this->addColumn('content', 'email', $this->string());
        $this->addColumn('content', 'shipping', $this->string());
        $this->addColumn('content', 'shipping_time', $this->string());
    }

    public function safeDown()
    {
        $this->addColumn('content', 'text', $this->string());
        $this->addColumn('content', 'label', $this->string(20)->unique());
        $this->dropColumn('content', 'header');
        $this->dropColumn('content', 'descriptor');
        $this->dropColumn('content', 'present_title');
        $this->dropColumn('content', 'present_description');
        $this->dropColumn('content', 'present_price');
        $this->dropColumn('content', 'disp_phone');
        $this->dropColumn('content', 'manager_phone');
        $this->dropColumn('content', 'email');
        $this->dropColumn('content', 'shipping');
        $this->dropColumn('content', 'shipping_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
      echo "m170830_065419_add_column_to_content_table cannot be reverted.\n";

      return false;
    }
    */
}
