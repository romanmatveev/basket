<?php

use yii\db\Migration;

class m170907_061621_add_foreign_keys_to_product_bonus_table extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-product_bonus-product_id',
            'product_bonus',
            'product_id',
            'product',
            'id'
        );

        $this->addForeignKey(
            'fk-bonus-bonus_id',
            'product_bonus',
            'bonus_id',
            'bonus',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-product-product_id',
            'product_bonus'
        );

        $this->dropForeignKey(
            'fk-bonus-bonus_id',
            'product_bonus'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_061621_add_foreign_keys_to_product_bonus_table cannot be reverted.\n";

        return false;
    }
    */
}
