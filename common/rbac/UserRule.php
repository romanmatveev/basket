<?php

namespace app\common\rbac;

use app\models\AuthAssignment;
use yii\rbac\Rule;
use yii\rbac\Item;

class UserRule extends Rule
{
    public $name = 'UserRule';

    /**
     * @param string|integer $user ID пользователя.
     * @param Item $item роль или разрешение с которым это правило ассоциировано
     * @param array $params параметры, переданные в ManagerInterface::checkAccess(), например при вызове проверки
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $user_id = \Yii::$app->user->id;
        $assign = AuthAssignment::find()
            ->where(['user_id' => $user_id])
            ->one();
        if($assign->item_name == 'admin') {
            return true;
        }
        return isset($params['profileId']) ? $user_id == $params['profileId'] : false;
    }
}