<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

$this->title = ($cake->name)?$cake->name:'Новый элемент';
?>
<div class="container">
    <h4>Страница редактирования пирожного</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <div class="blog-post">
                    <?php if (Yii::$app->session->hasFlash('cakeadd')): ?>
                        <p class="alert-danger">Изменения сохранены.</p>
                    <?php endif; ?>
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]) ?>
                    <div class="col-md-12">
                    <?= $form->field($cake, 'name')->textInput() ?>
                        </div>
                    <div class="col-md-12">
                    <?= $form->field($cake, 'composition')->textarea(['rows' => 5]) ?>
                    </div>
                    <div class="col-md-12">
                    <?= $form->field($cake, 'description')->textarea(['rows' => 5]) ?>
                    </div>
                    <p>Изображения:</p>
                    <?= $form->field($newImage, 'imageFile')->fileInput()->label('Добавить'); ?>
                    <?php foreach ($cake->cakeImages as $image) {
                        if (isset($image->image)) {
                            ?>
                            <img src="<?= '/' . $image->image; ?>" width="250px">
                            &nbsp<?= Html::a('Удалить', Url::to(['/cms/cake/delete-image', 'id' => $image->id, 'cake_id' => $cake->id, ])) ?>
                        <?php } ?>
                    <?php } ?>
                    <div>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                        &nbsp;&nbsp;
                        <?= Html::a('Back', Url::toRoute(["/cms/cakes/"]), ['class' => 'btn btn-info']) ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>