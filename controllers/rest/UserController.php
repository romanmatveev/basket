<?php

namespace app\controllers\rest;

use app\models\AuthItem;
use app\models\User;
use yii\rest\ActiveController;
use yii;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\AccessControl;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if ($user->validatePassword($password)) {
                    return $user;
                }
            },
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['apiPermission'],
                ],
            ],
            'denyCallback' => function () {
                throw new yii\web\NotAcceptableHttpException('Not enough permissions for this operations');
            },
        ];
        return $behaviors;
    }


    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionCreate()
    {
        $model = new User;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->authKey = Yii::$app->getRequest()->getBodyParam('authKey');
        $model->accessToken = Yii::$app->getRequest()->getBodyParam('accessToken');
        $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
        if ($model->save()) {
            $item = Yii::$app->getRequest()->getBodyParam('role');
            if ($item == '') {
                $role = User::getUserRole($model->id);
                $role->user_id = $model->id;
                $role->item_name = 'reader';
                $role->created_at = date('YmdH');
                $role->save();
            } elseif (AuthItem::findOne(['name' => $item])) {
                $role = User::getUserRole($model->id);
                $role->user_id = $model->id;
                $role->item_name = $item;
                $role->created_at = date('YmdH');
                $role->save();
            }
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::to('@web/rest/users/' . $id));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionUpdate($id)
    {

        $model = User::findOne(['id' => $id]);
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $password = Yii::$app->getRequest()->getBodyParam('password');
        if (isset($password)) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($password);
        }
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        } else {
            $item = Yii::$app->getRequest()->getBodyParam('role');
            if ($item == '') {
                $role = User::getUserRole($model->id);
                $role->user_id = $model->id;
                $role->item_name = 'reader';
                $role->created_at = date('YmdH');
                $role->save();
            } elseif (AuthItem::findOne(['name' => $item])) {
                $role = User::getUserRole($model->id);
                $role->user_id = $model->id;
                $role->item_name = $item;
                $role->created_at = date('YmdH');
                $role->save();
            }
        }

        return $model;
    }
}