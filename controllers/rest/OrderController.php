<?php

namespace app\controllers\rest;

use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use app\models\User;
use yii\filters\AccessControl;
use yii;

class OrderController extends ActiveController
{
    public $modelClass = 'app\models\Order';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if($user->validatePassword($password)) {
                    return $user;
                }
            },
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['apiPermission'],
                ],
            ],
            'denyCallback' => function () {
                throw new yii\web\NotAcceptableHttpException('Not enough permissions for this operations');
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }
}