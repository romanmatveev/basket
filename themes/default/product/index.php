<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Наборы сладостей';

?>
<div class="hero" style="display: none"></div>
<div style="height: 90px;"></div>
<div class="p-card">
    <a name="products" class="anchor"></a>
    <div class="p-card__container">
        <button class="p-card__slider-prev"></button>
        <button class="p-card__slider-next"></button>
        <div class="p-card__slider">
            <?php foreach ($products as $product) { ?>
                <div class="p-card__item">
                    <div class="p-card__main">
                        <div class="p-card__main-image">
                            <img src="<?= Html::encode($product->image) ?>" alt="">
                        </div>
                        <div class="p-card__content">
                            <span class="p-card__ttl"><?= Html::encode($product->name) ?></span>
                            <div class="p-card__ctr p-card__ctr--mobile">
                                <span class="p-card__ctr-current"><span class="p-card__ctr-num"></span> набор</span>
                                <span class="p-card__ctr-total">из <span class="p-card__ctr-num"></span></span>
                            </div>
                            <div class="p-card__content-image">
                                <img src="<?= $product->image ?>" alt="">
                            </div>
                            <ul class="p-card__descr">
                                <li class="p-card__descr-item">
                                    <span class="p-card__descr-name">Вес:</span>
                                    <span class="p-card__descr-value"><?= Html::encode($product->weight) ?> кг</span>
                                </li>
                                <li class="p-card__descr-item">
                                    <span class="p-card__descr-name">Цена:</span>
                                    <span class="p-card__descr-value"><?= Html::encode($product->price) ?>
                                        &#8381;</span>
                                </li>
                            </ul>
                            <hr class="p-card__hr">
                            <?php
                            $flag = false;
                            foreach ($product->bonuses as $bonus) {
                                if (isset($bonus))
                                    $flag = true;
                            }
                            if ($flag) { ?>
                                <div class="p-bonus">
                                <div class="p-bonus__info">
                                    При заказе корзинки бонус на выбор:
                                </div>
                                <ul class="p-bonus__list">
                                    <?php foreach ($product->bonuses as $bonus) { ?>
                                        <li class="p-bonus__item">
                                            <input type="radio" class="p-bonus__radio" name="bonuses<?= $product->id ?>"
                                                   id="bonuses<?= $product->id ?>-<?= $bonus->id ?>"
                                                   data-productId="<?= $product->id ?>"
                                                   data-bonusId="<?= $bonus->id ?>">
                                            <label class="p-bonus__label"
                                                   for="bonus<?= $product->id ?>-<?= $bonus->id ?>">
                                        <span class="p-bonus__image"
                                              style="background-image: url('<?= $bonus->image ?>');"></span>
                										<span class="p-bonus__text">
                											<span class="p-bonus__ico"></span>
                											<span
                                                                class="p-bonus__name"><?= Html::encode($bonus->name) ?></span>
                										</span>
                                            </label>
                                        </li>
                                    <?php } ?>
                                </ul>
                                </div><?php } ?>
                            <div class="p-card__buy-box">
                                <a href="javascript:ModalShow('modal-<?= $product->id ?>')"
                                   class="button button--l button--gradient p-card__buy p-card__buy--desktop"><span
                                        class="button__text">Заказать</span></a>
                                <div class="p-card__ctr p-card__ctr--desktop">
                                    <span class="p-card__ctr-current"><span
                                            class="p-card__ctr-num"></span> набор</span>
                                    <span class="p-card__ctr-total">из <span class="p-card__ctr-num"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="p-card__components">
            <div class="p-card__components-slider">
                <?php foreach ($products as $product) { ?>
                    <div class="p-one">
                        <div class="p-one__slider-container">
                            <button class="p-one__slider-prev"></button>
                            <button class="p-one__slider-next"></button>
                            <div class="p-one__slider">
                                <?php foreach ($product->cakes as $cake) { ?>
                                    <div class="p-one__item">
                                        <div class="p-one__item-container">
                                            <div class="p-one__image-container">
                                                <span
                                                    class="p-one__total"><?= \app\models\ProductCake::getQuanity($product->id, $cake->id) ?></span>
                                                <div class="p-one__image-list">
                                                    <?php foreach ($cake->cakeImages as $image) { ?>
                                                        <a class="p-one__image" href="<?= $image->image ?>"
                                                           style="background-image: url(<?= $image->image ?>);"></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="p-one__text">
                                                <span class="p-one__ttl"><?= Html::encode($cake->name) ?></span>
                                                <a href="javascript:ModalShow('modal-<?= $product->id ?>-<?= $cake->id ?>')"
                                                   class="p-one__ingredients">Посмотреть состав</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <a href="javascript:ModalShow('modal-<?= $product->id ?>')"
                           class="button button--l button--gradient p-card__buy p-card__buy--mobile">
                            <span class="button__text">Заказать</span>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($products as $product) { ?>
    <div class="modal" id="modal-<?= $product->id ?>" style="display: none">
        <div class="modal__out">
            <div class="modal__in">
                <div class="modal__win">
                    <div class="modal__container">
                        <a href="javascript:ModalHide('modal-<?= $product->id ?>')" class="modal__close"></a>
                        <div class="modal__content">
                            <div class="modal-checkout">
                                <h1>Оформить заказ</h1>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'order-form' . $product->id,
                                ]) ?>
                                <?= $form->field($order, 'product_id')->hiddenInput(['value' => $product->id])->label(false); ?>
                                <div class="field__item">
                                    <?= $form->field($order, 'username', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('username'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'phone', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('phone'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'address', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('address'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'email', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('email'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'comment', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('comment'),
                                        ],
                                    ])->textarea(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <?php
                                $flag = false;
                                foreach ($product->bonuses as $bonus) {
                                    if (isset($bonus)) {
                                        $flag = true;
                                        break;
                                    }
                                }
                                if ($flag) { ?>
                                    <div class="p-bonus p-bonus--modal">
                                        <div class="p-bonus__info">
                                            Выберите бонус:
                                        </div>
                                        <ul class="p-bonus__list">
                                            <?php
                                            $bonuses = $bonus->getBonusId($product->id);
                                            array_multisort($bonuses);
                                            ?>
                                            <?= $form->field($order, 'bonus_id')
                                                ->radioList(
                                                    $bonuses,
                                                    [
                                                        'item' => function ($index, $label, $name, $checked, $value) use ($product) {
                                                            $bonus = new \app\models\Bonus();
                                                            $bonus = $bonus->getBonus($label);
                                                            return
                                                                Html::tag('li',
                                                                    Html::radio($name, $checked, [
                                                                        'value' => $bonus->id,
                                                                        'class' => 'p-bonus__radio',
                                                                        'id' => 'bonus' . $product->id . '-' . $bonus->id
                                                                    ]) .
                                                                    Html::label(
                                                                        Html::tag('span', '', [
                                                                            'class' => 'p-bonus__image',
                                                                            'style' => 'background-image: url(' . $bonus->image . ')'
                                                                        ]) .
                                                                        Html::tag('span',
                                                                            Html::tag('span', '', [
                                                                                'class' => 'p-bonus__ico'
                                                                            ]) .
                                                                            Html::tag('span', Html::encode($bonus->name), [
                                                                                'class' => 'p-bonus__name'
                                                                            ]), [
                                                                                'class' => 'p-bonus__text'
                                                                            ]), 'bonus' . $product->id . '-' . $bonus->id, [
                                                                        'class' => 'p-bonus__label'
                                                                    ]), [
                                                                        'class' => 'p-bonus__item'
                                                                    ]);
                                                        }
                                                    ]
                                                )->label(false)
                                            ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <div class="field__item">
                                    <?= $form->field($order, 'reCaptcha')
                                        ->widget(\pixelycia\yii2\recaptcha\ReCaptcha::className(), [
                                            'widgetOptions' => [
                                                'id' => 'recaptcha-' . $product->id,
                                            ]
                                        ])->label(false) ?>
                                    <div style="display:none; color:#a94442" class="recaptcha_validate">
                                        Докажите, что вы не робот
                                    </div>
                                </div>

                                <button class="button button--l button--gradient modal-checkout__buy">
                                    <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
                                </button>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php foreach ($products as $product) { ?>
    <?php foreach ($product->cakes as $cake) { ?>
        <div class="modal" id="modal-<?= $product->id ?>-<?= $cake->id ?>" style="display: none">
            <div class="modal__out">
                <div class="modal__in">
                    <div class="modal__win">
                        <div class="modal__container">
                            <a href="javascript:ModalHide('modal-<?= $product->id ?>-<?= $cake->id ?>')"
                               class="modal__close"></a>
                            <div class="modal__content">
                                <div class="modal-ingredients">
                                    <h1><?= Html::encode($cake->name) ?></h1>
                                    <img src="<?= $cake->getFirstImage($cake->id) ?>"
                                         class="modal-ingredients__header-image" alt="">
                                    <h2>Описание</h2>
                                    <p><?= Html::encode($cake->description) ?></p>
                                    <p>&nbsp;</p>
                                    <h2>Состав:</h2>
                                    <p><?= Html::encode($cake->composition) ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>

