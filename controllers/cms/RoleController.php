<?php

namespace app\controllers\cms;

use yii\web\Controller;
use app\models\AuthItem;
use app\models\AuthItemChild;
use app\models\AuthAssignment;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Url;

class RoleController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'only' => ['index', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['updateRole'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteRole'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }

    public function actionView($name = null)
    {
        if (isset($name)) {
            $item = AuthItem::find()
                ->where(['name' => $name])
                ->one();

            if ($item->load(Yii::$app->request->post()) && $item->save()) {
                if($checkboxes = Yii::$app->request->post('checkboxes')) {

                    AuthItemChild::deleteAll(['parent' => $item->name]);

                    foreach ($checkboxes as $checkbox) {

                        $relation = new AuthItemChild();
                        $relation->parent = $item->name;
                        $relation->child = $checkbox;
                        $relation->save();
                    }
                }
                Yii::$app->session->setFlash('rolewaschanged');
            }
            
            $rules = AuthItemChild::find()
                ->select ('child')
                ->where(['parent' => $item->name])
                ->indexBy('child')
                ->column();
        } else {
            $item = new AuthItem();
            if ($item->load(Yii::$app->request->post())) {
                $item->type = 1;
                $item->created_at = date('YmdH');
                $item->updated_at = date('YmdH');
                $item->save();

                if($checkboxes = Yii::$app->request->post('checkboxes')) {
                    AuthItemChild::deleteAll(['parent' => $item->name]);
                    foreach ($checkboxes as $checkbox) {
                        $relation = new AuthItemChild();
                        $relation->parent = $item->name;
                        $relation->child = $checkbox;
                        $relation->save();
                    }
                }

                return $this->redirect(Url::to(['cms/role/view', 'name' => $item->name]));
            }
        }
        
        $permissions = AuthItem::find()
            ->select(['name'])
            ->where(['type' => 2])
            ->indexBy('name')
            ->column();

        return $this->render('view', compact('item', 'permissions', 'rules'));
    }

    public function actionDelete($name)
    {
        (AuthItem::deleteItem($name)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/users');
    }
}