<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170829_083033_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'weight' => $this->double(),
            'price' => $this->integer(),
            'image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
