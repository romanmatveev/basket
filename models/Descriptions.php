<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Descriptions extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{descriptions}}';
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Надпись',
            'label' => 'Позиция',
        ];
    }

    public function getDescriptions()
    {
        return $descriptions = Descriptions::find()
            ->orderBy('id')
            ->all();
    }

    public function getDescription($label)
    {
        return $description = Descriptions::findOne(
            [
                'label' => $label,
            ]
        );
    }
}