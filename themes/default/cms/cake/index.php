<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Пирожные';

?>
<div class="container">
    <h4>Список пирожных</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <?php if (Yii::$app->session->hasFlash('deleted')): ?>
                    <p class="alert-danger">Объект удален.</p>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
                    <p class="alert-danger">Ошибка при удалении!</p>
                <?php endif; ?>
                <table class="table">
                    <tbody>
                    <?php foreach ($cakes as $cake) { ?>
                        <tr>
                            <td>
                                <?php if (\Yii::$app->user->can('updateCake')) { ?>
                                    <?= Html::a(Html::encode($cake->name), Url::to(['/cms/cake/cake', 'id' => $cake->id])) ?>
                                <?php } else {
                                    echo Html::encode($cake->name);
                                } ?>
                            </td>
                            <?php if (\Yii::$app->user->can('updateCake')) { ?>
                                <td>
                                    <?= Html::a('Изменить', Url::to(['/cms/cake/cake', 'id' => $cake->id]), ['class' => 'btn btn-primary']) ?>
                                </td>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('deleteCake')) { ?>
                                <td>
                                    <?= Html::a('Удалить', Url::to(['/cms/cake/delete-cake', 'id' => $cake->id]), ['class' => 'btn btn-danger']) ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <?php if (\Yii::$app->user->can('createCake')) { ?>
                        <tr>
                            <td>
                                <?= Html::a('Новое пирожное', Url::to(['/cms/cake/cake']), ['class' => 'btn btn-success']) ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>