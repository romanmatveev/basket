<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Бонусы';

?>
<div class="container">
    <?php if (Yii::$app->session->hasFlash('deleted')): ?>
        <p class="alert-danger">Объект удален.</p>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('not_deleted')): ?>
        <p class="alert-danger">Ошибка при удалении!</p>
    <?php endif; ?>
    <h4>Список бонусов</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <table class="table">
                    <tbody>
                    <?php foreach ($bonuses as $bonus) { ?>
                        <tr>
                            <td>
                                <?php if (\Yii::$app->user->can('updateBonus')) { ?>
                                    <?= Html::a(Html::encode($bonus->name), Url::to(['/cms/bonus/view', 'id' => $bonus->id])) ?>
                                <?php } else {
                                    echo Html::encode($bonus->name);
                                } ?>
                            </td>
                            <?php if (\Yii::$app->user->can('updateBonus')) { ?>
                                <td>
                                    <?= Html::a('Изменить', Url::to(['/cms/bonus/view', 'id' => $bonus->id]), ['class' => 'btn btn-primary']) ?>
                                </td>
                            <?php } ?>
                            <?php if (\Yii::$app->user->can('deleteBonus')) { ?>
                                <td>
                                    <?= Html::a('Удалить', Url::to(['/cms/bonus/delete', 'id' => $bonus->id]), ['class' => 'btn btn-danger']) ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    <?php if (\Yii::$app->user->can('createBonus')) { ?>
                        <tr>
                            <td>
                                <?= Html::a('Новый бонус', Url::to(['/cms/bonus/view']), ['class' => 'btn btn-success']) ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>