<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cake`.
 */
class m170830_062653_create_cake_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cake', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'composition' => $this->string(2500),
            'image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cake');
    }
}
