<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use app\models\Content;

AppAsset::register($this);

$ip_geo = Yii::$app->ipgeobase->getLocation('91.191.250.154'/*Yii::$app->request->userIP*/);

$contnt = new Content();
$contnt = $contnt->getContent();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header class="header">
    <div class="h-menu">
        <div class="h-menu__container">

            <div class="h-menu__logo-box">
                <div class="h-menu-toggle">
                    <button class="h-menu-toggle__button"></button>
                </div>
                <div class="h-logo">
                    <?= Html::a(
                            Html::img('themes/default/img/logo-ks-150x56.svg', ['alt' => 'Корзинка Сладостей', 'class' => 'h-logo__image']),
                            Url::to(['/']),
                        ['class' => 'h-logo__link']) ?>
                </div>
            </div>
            <div class="h-menu__main">
                <div class="h-menu__main-content">
                    <nav class="nav">
                        <ul class="nav__list">
                            <li class="nav__item">
                                <?= Html::a('Главная', Url::to(['/']), ['class' => 'nav__button']) ?>
                            </li>
                            <li class="nav__item">
                                <?= Html::a('Сценарии', Url::to(['/#usecase']), ['class' => 'nav__button']) ?>
                            </li>
                            <li class="nav__item">
                                <?= Html::a('Преимущества', Url::to(['/#benefits']), ['class' => 'nav__button']) ?>
                            </li>
                            <li class="nav__item">
                                <?= Html::a('Наборы', Url::to(['/products']), ['class' => 'nav__button']) ?>
                            </li>

                            <li class="nav__item">
                                <?= Html::a('Подар. наборы', Url::to(['/giftsets']), ['class' => 'nav__button']) ?>
                            </li>
                            <li class="nav__item">
                                <?= Html::a('Контакты', Url::to(['/contacts']), ['class' => 'nav__button']) ?>
                            </li>
                        </ul>
                    </nav>
                    <ul class="h-address">
                        <li class="h-address__item h-address__item--phone"><?= $contnt->disp_phone ?></li>
                        <li class="h-address__item h-address__item--city">
                            г. <span class="h-address__city"><?= $ip_geo['city']; ?></span>
                        </li>
                    </ul>
                </div>
                <div class="h-menu__main-overlay"></div>
            </div>
        </div>
    </div>
</header>


<?= $content ?>

<footer class="footer">
    <a href="#" class="scrollup"></a>
    <div class="footer__container">
        <div class="footer__left">
            <span><?= date("Y")?> © ООО "Корзинка сладостей" Доставка кондитерских изделий</span>
        </div>
        <div class="footer__right">
            <ul class="f-soc">
                <li class="f-soc__item f-soc__item--f">
                    <a href="#" class="f-soc__button"></a>
                </li>
                <li class="f-soc__item f-soc__item--v">
                    <a href="#" class="f-soc__button"></a>
                </li>
                <li class="f-soc__item f-soc__item--t">
                    <a href="#" class="f-soc__button"></a>
                </li>
                <li class="f-soc__item f-soc__item--i">
                    <a href="#" class="f-soc__button"></a>
                </li>
            </ul>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
