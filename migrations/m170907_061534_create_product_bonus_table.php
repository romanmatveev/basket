<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_bonus`.
 */
class m170907_061534_create_product_bonus_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_bonus', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'bonus_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_bonus');
    }
}
