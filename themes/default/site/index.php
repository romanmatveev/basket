<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Корзинка сладостей';

$ip_geo = Yii::$app->ipgeobase->getLocation(Yii::$app->request->userIP);


?>

<div class="hero">
    <div class="hero__content">
        <div class="hero__text">
            <span class="hero__ttl hero__ttl--1">
                <?= Html::encode($content->header); ?>
            </span>
			<span class="hero__ttl hero__ttl--2">
                <?= Html::encode($content->descriptor) ?>
            </span>
            <a href="javascript:ModalShow('modal-top')" class="button button--l button--white">Заказать</a>
        </div>
    </div>
    <div class="hero__media">
        <div class="hero-parallax">
            <div class="hero-parallax__layer-container hero-parallax__layer-container--4">
                <div class="hero-parallax__layer hero-parallax__layer--4 layer" data-depth="0.05"></div>
            </div>
            <div class="hero-parallax__layer-container hero-parallax__layer-container--3">
                <div class="hero-parallax__layer hero-parallax__layer--3 layer" data-depth="0.1"></div>
            </div>
            <div class="hero-parallax__layer-container hero-parallax__layer-container--2">
                <div class="hero-parallax__layer hero-parallax__layer--2 layer" data-depth="0.2"></div>
            </div>
            <div class="hero-parallax__layer-container hero-parallax__layer-container--1">
                <div class="hero-parallax__layer hero-parallax__layer--1 layer" data-depth="0.6"></div>
            </div>
        </div>
    </div>
</div>


<div class="usecase">
    <a name="usecase" class="anchor"></a>
    <div class="usecase__container">
        <button class="usecase__slider-prev"></button>
        <button class="usecase__slider-next"></button>
        <div class="usecase__slider-wrapper">
            <div class="usecase__slider">
                <?php foreach ($usecases as $case) { ?>
                    <div class="usecase__item">
                        <div class="usecase__image-container">
                            <div class="usecase__image"
                                 style="background-image: url(<?= $case->image ?>);"></div>
                        </div>
                        <div class="usecase__text"><?= \yii\helpers\HtmlPurifier::process($case->name) ?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="usecase__slider-dots"></div>
</div>
<div class="benefits">
    <a name="benefits" class="anchor"></a>
    <div class="benefits__container">
        <ul class="benefits__list">
            <li class="benefits__item">
                <div class="benefits__image"
                     style="background-image: url(themes/default/img/ico-230x200-benefits-product.png);"></div>
                <span class="benefits__text">Сделано из свежих и натуральных продуктов</span>
            </li>
            <li class="benefits__item">
                <div class="benefits__image"
                     style="background-image: url(themes/default/img/ico-230x200-benefits-box.png);"></div>
                <span class="benefits__text">Все расфасовано в жесткие коробки и не мнется</span>
            </li>
            <li class="benefits__item">
                <div class="benefits__image"
                     style="background-image: url(themes/default/img/ico-230x200-benefits-kit.png);"></div>
                <span class="benefits__text">В наборе есть тарелки, салфетки и перчатки для удобства</span>
            </li>
        </ul>
    </div>
</div>


<div class="modal" id="modal-top" style="display: none">
    <div class="modal__out">
        <div class="modal__in">
            <div class="modal__win">
                <div class="modal__container">
                    <a href="javascript:ModalHide('modal-top')" class="modal__close"></a>
                    <div class="modal__content">
                        <div class="modal-checkout">
                            <h1>Оформить заказ</h1>
                            <?php $form = ActiveForm::begin([
                                'id' => 'order-form',
                            ]) ?>
                            <?= $form->field($order, 'product_id')->hiddenInput(['value' => null])->label(false); ?>
                            <?= $form->field($order, 'bonus_id')->hiddenInput(['value' => null])->label(false); ?>
                            <?= $form->field($order, 'present')->hiddenInput(['value' => null])->label(false); ?>
                            <div class="field__item">
                                <?= $form->field($order, 'username', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('username'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'phone', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('phone'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'address', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('address'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'email', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('email'),
                                    ],
                                ])->textInput(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'comment', [
                                    'inputOptions' => [
                                        'placeholder' => $order->getAttributeLabel('comment'),
                                    ],
                                ])->textarea(['class' => 'field__elem'])->label(false) ?>
                            </div>
                            <div class="field__item">
                                <?= $form->field($order, 'reCaptcha')
                                    ->widget(\pixelycia\yii2\recaptcha\ReCaptcha::className(), [
                                        'widgetOptions' => [
                                            'id' => 'recaptcha-top',
                                        ]
                                    ])->label(false) ?>
                                <div style="display:none; color:#a94442" class="recaptcha_validate">
                                    Докажите, что вы не робот
                                </div>
                            <button class="button button--l button--gradient modal-checkout__buy">
                                <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
                            </button>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



