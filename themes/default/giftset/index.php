<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Подарочные наборы';

?>
    <div class="hero" style="display: none"></div>
    <div style="height: 90px;"></div>
    <div class="present">
        <a name="present" class="anchor"></a>
        <div class="present__container">
            <div class="present__image"></div>
            <div class="present__content">
                <span class="present__ttl"><?= Html::encode($content->present_title) ?></span>
                <p><?= Html::encode($content->present_description) ?></p>
                <p>&nbsp;</p>
                <div class="present__price">
                    <span class="present__price-name">Стоимость услуги:</span>
                    <span class="present__price-value"><?= Html::encode($content->present_price) ?>&#8381;</span>
                </div>
                <a href="javascript:ModalShow('modal-0')" class="button button--l button--gradient"><span
                        class="button__text">Заказать</span></a>
            </div>
        </div>
    </div>
    <div class="p-set">
        <a name="gallery" class="anchor"></a>
        <div class="p-set__container">
            <div class="p-set__list">
                <?php foreach ($products as $product) { ?>
                    <div class="p-set__item">
                        <div class="p-set__main">
                            <a class="p-set__image" href="<?= $product->image ?>"
                               style="background-image: url(<?= $product->image ?>);"></a>
                            <a href="javascript:ModalShow('modal-<?= $product->id ?>')" class="p-set__content">
                                <div class="p-set__content-wrapper">
						<span class="p-set__title">
							<span class="p-set__title-small">Подарочный набор</span>
							<span class="p-set__title-big"><?= Html::encode($product->name) ?></span>
						</span>
                                    <span class="button button--l button--white p-set__buy">Заказать</span>
                                </div>
                            </a>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
    <div class="modal" id="modal-0" style="display: none">
        <div class="modal__out">
            <div class="modal__in">
                <div class="modal__win">
                    <div class="modal__container">
                        <a href="javascript:ModalHide('modal-0')" class="modal__close"></a>
                        <div class="modal__content">
                            <div class="modal-checkout">
                                <h1>Оформить заказ</h1>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'order-form',
                                ]) ?>
                                <div class="field__item">
                                    <?= $form->field($order, 'username', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('username'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'phone', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('phone'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'address', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('address'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'email', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('email'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'comment', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('comment'),
                                        ],
                                    ])->textarea(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'reCaptcha')
                                        ->widget(\pixelycia\yii2\recaptcha\ReCaptcha::className(), [
                                            'widgetOptions' => [
                                                'id' => 'recaptcha-top',
                                            ]
                                        ])->label(false) ?>
                                    <div style="display:none; color:#a94442" class="recaptcha_validate">
                                        Докажите, что вы не робот
                                    </div>
                                </div>
                                <button class="button button--l button--gradient modal-checkout__buy">
                                    <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
                                </button>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php foreach ($products as $product) { ?>
    <div class="modal" id="modal-<?= $product->id ?>" style="display: none">
        <div class="modal__out">
            <div class="modal__in">
                <div class="modal__win">
                    <div class="modal__container">
                        <a href="javascript:ModalHide('modal-<?= $product->id ?>')" class="modal__close"></a>
                        <div class="modal__content">
                            <div class="modal-checkout">
                                <h1>Оформить заказ</h1>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'order-form' . $product->id,
                                ]) ?>
                                <?= $form->field($order, 'product_id')->hiddenInput(['value' => $product->id])->label(false); ?>
                                <div class="field__item">
                                    <?= $form->field($order, 'username', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('username'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'phone', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('phone'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'address', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('address'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'email', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('email'),
                                        ],
                                    ])->textInput(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <div class="field__item">
                                    <?= $form->field($order, 'comment', [
                                        'inputOptions' => [
                                            'placeholder' => $order->getAttributeLabel('comment'),
                                        ],
                                    ])->textarea(['class' => 'field__elem'])->label(false) ?>
                                </div>
                                <?php
                                $flag = false;
                                foreach ($product->bonuses as $bonus) {
                                    if (isset($bonus)) {
                                        $flag = true;
                                        break;
                                    }
                                }
                                if ($flag) { ?>
                                    <div class="p-bonus p-bonus--modal">
                                        <div class="p-bonus__info">
                                            Выберите бонус:
                                        </div>
                                        <ul class="p-bonus__list">
                                            <?php
                                            $bonuses = $bonus->getBonusId($product->id);
                                            array_multisort($bonuses);
                                            ?>
                                            <?= $form->field($order, 'bonus_id')
                                                ->radioList(
                                                    $bonuses,
                                                    [
                                                        'item' => function ($index, $label, $name, $checked, $value) use ($product) {
                                                            $bonus = new \app\models\Bonus();
                                                            $bonus = $bonus->getBonus($label);
                                                            return
                                                                Html::tag('li',
                                                                    Html::radio($name, $checked, [
                                                                        'value' => $bonus->id,
                                                                        'class' => 'p-bonus__radio',
                                                                        'id' => 'bonus' . $product->id . '-' . $bonus->id
                                                                    ]) .
                                                                    Html::label(
                                                                        Html::tag('span', '', [
                                                                            'class' => 'p-bonus__image',
                                                                            'style' => 'background-image: url(' . $bonus->image . ')'
                                                                        ]) .
                                                                        Html::tag('span',
                                                                            Html::tag('span', '', [
                                                                                'class' => 'p-bonus__ico'
                                                                            ]) .
                                                                            Html::tag('span', Html::encode($bonus->name), [
                                                                                'class' => 'p-bonus__name'
                                                                            ]), [
                                                                                'class' => 'p-bonus__text'
                                                                            ]), 'bonus' . $product->id . '-' . $bonus->id, [
                                                                        'class' => 'p-bonus__label'
                                                                    ]), [
                                                                        'class' => 'p-bonus__item'
                                                                    ]);
                                                        }
                                                    ]
                                                )->label(false)
                                            ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <div class="field__item">
                                    <?= $form->field($order, 'reCaptcha')
                                        ->widget(\pixelycia\yii2\recaptcha\ReCaptcha::className(), [
                                            'widgetOptions' => [
                                                'id' => 'recaptcha-' . $product->id,
                                            ]
                                        ])->label(false) ?>
                                    <div style="display:none; color:#a94442" class="recaptcha_validate">
                                        Докажите, что вы не робот
                                    </div>
                                </div>
                                <button class="button button--l button--gradient modal-checkout__buy">
                                    <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
                                </button>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>