<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;



/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppCms extends AssetBundle
{
    public $basePath = '@webroot/themes/default';
    public $baseUrl = '@web/themes/default';

    public $css = [
        'css/main.min.css',
        'css/bootstrap.min.css',
        'css/blog.css',
    ];
    public $js = [
		'js/main.min.js',
        'js/ie10-viewport-bug-workaround.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}