<?php

namespace app\controllers\cms;

use app\models\Usecase;
use yii\web\UploadedFile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;

class UsecaseController extends Controller
{
    public $layout = 'cms';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['readUsecase'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['updateUsecase'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteUsecase'],
                    ],
                ],
                'denyCallback' => function () {
                    if (Yii::$app->user->getIsGuest()){
                        return $this->redirect('@web/cms/login');
                    } else {
                        return $this->redirect('@web/cms/access-deny');
                    }
                },
            ],
        ];
    }
    
    public function actionIndex()
    {
        $usecases = Usecase::find()
            ->orderBy('id')
            ->all();
        return $this->render('index', compact('usecases'));
    }

    public function actionView($id = null)
    {
        if (isset($id)) {
            $case = Usecase::find()
                ->where(['id' => $id])
                ->one();
        } else {
            $case = new Usecase();
        }

        if (Yii::$app->request->isPost) {

            if ($case->load(Yii::$app->request->post()) && $case->validate()) {
                $case->save();
                //Yii::$app->session->setFlash('cakeadd');
            }

            $case->imageFile = UploadedFile::getInstance($case, 'imageFile');

            if ($case->upload()) {
                Yii::$app->session->setFlash('cakeadd');
//                $bonus->image = 'themes/default/img/' . $newImage->imageFile->baseName . '.' . $newImage->imageFile->extension;
                $case->save(false);
                $case->imageFile = '';
            }
        }
        //redirect for new item
        if ((Url::to() == '/landing/web/cms/usecases/view') && $case->id) {
            return $this->redirect(Url::to(['cms/usecas/view', 'id' => $case->id]));
        }
        return $this->render('view', compact('case'));
    }

    public function actionDelete($id)
    {
        (Usecase::deleteItem($id)) ? Yii::$app->session->setFlash('deleted') : Yii::$app->session->setFlash('not_deleted');
        return $this->redirect('@web/cms/usecases');
    }
}