<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\Linkable;
use yii\helpers\Url;
use yii\web\Link;

/**
 * This is the model class for table "product".
 *
 * @property integer   $id
 * @property string    $name
 * @property double    $weight
 * @property integer   $price
 * @property string    $image
 */
class Product extends ActiveRecord implements Linkable
{
    public $imageFile;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{product}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'weight', 'price'], 'required'],
            ['name', 'string', 'max' => 100],
            ['weight', 'double', 'min' => 0],
            ['price', 'integer', 'min' => 0],
            ['image', 'string'],
            [['imageFile'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'weight' => 'Вес',
            'price' => 'Цена',
            'image' => 'Изображение',
        ];
    }

    public static function getPrice($id)
    {
        $product = Product::findOne(['id' => $id]);
        return $product->price;
    }

    public function getCakes()
    {
        return $this->hasMany(Cake::className(), ['id' => 'cake_id'])
            ->viaTable('product_cake', ['product_id' => 'id']);
    }

    public function getProductCake()
    {
        return $this->hasMany(ProductCake::className(), ['product_id' => 'id']);
    }

    public function getBonuses()
    {
        return $this->hasMany(Bonus::className(), ['id' => 'bonus_id'])
            ->viaTable('product_bonus', ['product_id' => 'id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['product_id' => 'id']);
    }

    public function upload()
    {
        if ($this->validate() && isset($this->imageFile)) {
            $this->imageFile->saveAs('themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = 'themes/default/img/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    public static function deleteBonus($product_id, $bonus_id)
    {
        return (ProductBonus::deleteAll([
            'product_id' => $product_id,
            'bonus_id' => $bonus_id
        ])) ? true : false;
    }

    public static function deleteCake($product_id, $cake_id)
    {
        return (ProductCake::deleteAll([
            'product_id' => $product_id,
            'cake_id' => $cake_id
        ])) ? true : false;
    }

    public static function deleteItem($id)
    {
        $product = Product::findOne(['id' => $id]);
        return ($product->delete()) ? true : false;
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['rest/products/' . $this->id], true),
            Link::REL_LIST => Url::to(['rest/products/'], true),
        ];
    }
}