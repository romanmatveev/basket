<?php

use yii\db\Migration;

class m171004_081739_add_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'allowance', $this->integer());
        $this->addColumn('user', 'allowance_updated_at', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'allowance');
        $this->dropColumn('user', 'allowance_updated_at');
    }
}
