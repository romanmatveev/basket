<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

$this->title = 'Бонус';
?>
<div class="container">
    <h4>Страница редактирования бонуса</h4>
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <div class="blog-post">
                    <?php if (Yii::$app->session->hasFlash('cakeadd')): ?>
                        <p class="alert-danger">Изменения сохранены.</p>
                    <?php endif; ?>
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]) ?>
                    <div class="col-md-12">
                        <?= $form->field($bonus, 'name')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?php if (isset($bonus->image)) { ?>
                            <p>Изображение:</p>
                            <img src="<?= '/landing/web/' . $bonus->image; ?>" width="200">
                            <?= $form->field($bonus, 'imageFile')->fileInput()->label('Изменить:'); ?>
                        <?php } else { ?>
                            <p>Изображение отсутствует.</p>
                            <?= $form->field($bonus, 'imageFile')->fileInput()->label('Добавить:'); ?>
                        <?php } ?>
                    </div>
                    <div>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                        &nbsp;&nbsp;
                        <?= Html::a('Back', Url::toRoute(["/cms/bonuses/"]), ['class' => 'btn btn-info']) ?>
                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>