<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\VarDumper;

$this->title = 'Заказы';

?>
<div class="container">
    <div class="row">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    ФИО
                </td>
                <td>
                    Телефон
                </td>
                <td>
                    Email
                </td>
                <td>
                    Адрес
                </td>
                <td>
                    Комментарий
                </td>
                <td>
                    Товар
                </td>
                <td>
                    Бонус
                </td>
                <td>
                    Цена
                </td>
                <td>
                    Подарок
                </td>

                <td>
                    Итого
                </td>
            </tr>
            <?php foreach ($orders as $order) { ?>
                <tr>
                    <td>
                        <?= Html::encode($order->username) ?>
                    </td>
                    <td>
                        <?= Html::encode($order->phone) ?>
                    </td>
                    <td>
                        <?= Html::encode($order->email) ?>
                    </td>
                    <td>
                        <?= Html::encode($order->address) ?>
                    </td>
                    <td>
                        <?= Html::encode($order->comment) ?>
                    </td>
                    <td>
                        <?php if (isset($order->product_id)) { ?>
                        <?= Html::encode($order->products->name) ?>
                        <?php } else { ?>
                            -
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (isset($order->bonus_id)) { ?>
                            <?= Html::encode($order->bonuses->name) ?>
                        <?php } else { ?>
                            -
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (isset($order->product_id)) { ?>
                            <?= Html::encode($order->products->price) ?>
                        <?php } else { ?>
                            -
                        <?php } ?>
                    </td>
                    <td align="center">
                        <?php if ($order->present == 1) {?>
                        <input type="checkbox" checked disabled>
                        <?php } else { ?>
                        <input type="checkbox" disabled>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (isset($order->total)) {
                            echo Html::encode($order->total);
                        } else {
                            echo '-';
                        } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
