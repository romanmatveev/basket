<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Content таблица content
 * @property integer $id
 * @property string  $header
 * @property string  $descriptor
 * @property string  $present_title
 * @property string  $present_description
 * @property string  $present_price
 * @property string  $disp_phone
 * @property string  $manager_phone
 * @property string  $email
 * @property string  $shipping
 * @property string  $shipping_time
 * @package app\models
 */
class Content extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{content}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['header', 'descriptor', 'present_title', 'present_description', 'present_price', 'disp_phone', 'manager_phone', 'email', 'shipping', 'shipping_time',], 'required'],
            [['header', 'descriptor', 'present_title', 'present_description', 'shipping', 'shipping_time'], 'string', 'min' => 3],
            ['present_price', 'integer', 'min' => 0],
            ['email', 'email'],
            [['disp_phone', 'manager_phone'], 'match', 'pattern' => '/((8|\+7)(\-|\ )?)?\(?\d{3}\)?(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}(\-|\ )?\d{1}$/'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'header'              => 'Заголовок',
            'descriptor'          => 'Дескриптор',
            'present_title'       => 'Заголовок услуги',
            'present_description' => 'Описание услуги',
            'present_price'       => 'Стоимость подарочной упаковки',
            'disp_phone'          => 'Телефон диспетчера',
            'manager_phone'       => 'Телефон менеджера',
            'email'               => 'Электронная почта',
            'shipping'            => 'Условия доставки',
            'shipping_time'       => 'Срок доставки',
        ];
    }

    public function getContent($id = null)
    {
        if ($id) {
            return $content = Content::findOne(
                [
                    'id' => $id,
                ]
            );
        } else {
            return $content = Content::find()
                ->orderBy('id DESC')
                ->one();
        }
    }

}