<?php

namespace app\models;


use yii\db\ActiveRecord;

class AuthAssignment extends ActiveRecord
{
    public static function tableName()
    {
        return '{{auth_assignment}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            ['item_name', 'string'],
            ['user_id', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
        ];
    }

    public function getUser()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id']);
    }
}