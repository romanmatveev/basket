<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Контакты';

?>
<div class="hero" style="display: none"></div>
<div style="height: 90px;"></div>
<div class="contacts">
    <a name="contacts" class="anchor"></a>
    <div class="contacts__map">
        <div class="contacts__ymap" id="map"></div>
    </div>
    <div class="contacts__main">
        <div class="contacts__content">
            <ul class="tab__nav tab__nav--contacts">
                <li class="tab__nav-item tab__nav-item--active">Контакты</li>
                <li class="tab__nav-item">Доставка</li>
            </ul>
            <div class="tab__content-container tab__content-container--contacts">
                <div class="tab__content tab__content--active">
                    <ul class="contacts__list">
                        <li>Диспетчер: <?= Html::encode($content->disp_phone) ?></li>
                        <li>Менеджер: <?= Html::encode($content->manager_phone) ?></li>
                        <li>E-mail: <?= Html::encode($content->email) ?></li>
                    </ul>
                </div>
                <div class="tab__content">
                    <p><?= Html::encode($content->shipping) ?></p>
                    <p><?= Html::encode($content->shipping_time) ?></p>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'options' => ['class' => 'contacts__form form--white'],
            ]) ?>
            <div class="field__item">
                <?= $form->field($order, 'username', [
                    'inputOptions' => [
                        'placeholder' => $order->getAttributeLabel('username'),
                    ],
                ])->textInput(['class' => 'field__elem'])->label(false) ?>
            </div>
            <div class="field__item">
                <?= $form->field($order, 'phone', [
                    'inputOptions' => [
                        'placeholder' => $order->getAttributeLabel('phone'),
                    ],
                ])->textInput(['class' => 'field__elem'])->label(false) ?>
            </div>
            <div class="field__item">
                <?= $form->field($order, 'address', [
                    'inputOptions' => [
                        'placeholder' => $order->getAttributeLabel('address'),
                    ],
                ])->textInput(['class' => 'field__elem'])->label(false) ?>
            </div>
            <div class="field__item">
                <?= $form->field($order, 'email', [
                    'inputOptions' => [
                        'placeholder' => $order->getAttributeLabel('email'),
                    ],
                ])->textInput(['class' => 'field__elem'])->label(false) ?>
            </div>
            <div class="field__item">
                <?= $form->field($order, 'comment', [
                    'inputOptions' => [
                        'placeholder' => $order->getAttributeLabel('comment'),
                    ],
                ])->textarea(['class' => 'field__elem'])->label(false) ?>
            </div>
            <div class="field__item">
                <?= $form->field($order, 'reCaptcha')
                    ->widget(\pixelycia\yii2\recaptcha\ReCaptcha::className(), [
                        'widgetOptions' => [
                            'id' => 'reCaptcha-contact',
                        ]
                    ])->label(false) ?>
                <div style="display:none; color:#a94442" class="recaptcha_validate">
                    Докажите, что вы не робот
                </div>
            </div>
            <button class="button button--l button--gradient contacts__buy">
                <?= Html::tag('span', 'Заказать', ['class' => 'button__text']) ?>
            </button>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>