<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Контент';

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 blog-main">
            <div class="blog-post">
                <h3>Контент сайта</h3>
                <?php if (Yii::$app->session->hasFlash('changessaved')): ?>
                    <p class="alert-danger">Изменения сохранены.</p>
                <?php endif; ?>
                <?php
                $form = ActiveForm::begin([
                    'id' => 'text1-form',
                    'fieldConfig' => [
                        'template' => "<div class=\"span2\">{label}:&nbsp</div><div class=\"span10 offset2\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    ],
                ]) ?>
                <div class="col-md-12">
                    <?= $form->field($content, 'header', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('header'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'descriptor', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('descriptor'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'disp_phone', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('disp_phone'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'manager_phone', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('manager_phone'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                    <?= $form->field($content, 'email', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('email'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'present_title', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('present_title'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'present_description', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('present_description'),
                        ],
                    ])->textarea(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'present_price', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('present_price'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'shipping', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('shipping'),
                        ],
                    ])->textInput(['class' => 'col-md-12']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($content, 'shipping_time', [
                        'inputOptions' => [
                            'placeholder' => $content->getAttributeLabel('shipping_time'),
                        ],
                    ])->textarea(['class' => 'col-md-12']) ?>
                </div>
                <?php if (\Yii::$app->user->can('updateContent')) { ?>
                    <div>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>