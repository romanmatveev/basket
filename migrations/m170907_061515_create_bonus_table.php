<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bonus`.
 */
class m170907_061515_create_bonus_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bonus', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->unique(),
            'image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bonus');
    }
}
