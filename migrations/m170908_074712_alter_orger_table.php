<?php

use yii\db\Migration;

class m170908_074712_alter_orger_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('orders', 'product_id', $this->integer());
        $this->addColumn('orders', 'bonus_id', $this->integer());
        $this->addColumn('orders', 'present', $this->boolean());
        $this->addColumn('orders', 'total', $this->integer());

        $this->addForeignKey(
            'fk-order-product_id',
            'orders',
            'product_id',
            'product',
            'id'
        );
        $this->addForeignKey(
            'fk-order-bonus_id',
            'orders',
            'bonus_id',
            'bonus',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('orders', 'product_id');
        $this->dropColumn('orders', 'bonus_id');
        $this->dropColumn('orders', 'present');
        $this->dropColumn('orders', 'total');

        $this->dropForeignKey(
            'fk-order-product_id',
            'orders'
        );
        $this->dropForeignKey(
            'fk-order-bonus_id',
            'orders'
        );
    }
}
